package com.dating.app.mycallback;

public interface AsyncResponse {

    public void processFinished(String data);
}
