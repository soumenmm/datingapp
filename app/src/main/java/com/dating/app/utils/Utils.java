package com.dating.app.utils;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.util.TypedValue;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dating.app.R;
import com.dating.app.activity.LoginActivity;
import com.dating.app.activity.MainIntroActivity;
import com.dating.app.activity.SignUpActivity;
import com.dating.app.model.dropdown.SpinResponse;
import com.dating.app.model.dropdown.SpinValue;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {


    public static int dpToPx(int dp,Context context) {
        Resources r = context.getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    public ArrayAdapter getDropDownAdapter(Context context, ArrayList<SpinValue> item) {
        ArrayAdapter dataAdapter = new ArrayAdapter(context, android.R.layout.simple_spinner_item, item);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        return dataAdapter;
    }

    public static void snakBar(View v, String msg) {
        Snackbar snackbar = Snackbar
                .make(v, msg, Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    public static boolean isConnectingToInternet(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }

        }
        return false;
    }


    public static void OpenDatePicker(final EditText edittext, Context context) {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        // Launch Date Picker Dialog
        DatePickerDialog dpd = new DatePickerDialog(context,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        // Display Selected date in textbox
                        edittext.setText(dayOfMonth + "-"
                                + (monthOfYear + 1) + "-" + year);

                    }
                }, mYear, mMonth, mDay);
        dpd.show();
    }


    public static void alert(final Context context, String msg) {
        AlertDialog alertDialog = new AlertDialog.Builder(
                context).create();

        alertDialog.setTitle(context.getString(R.string.app_name));

        // Setting Dialog Message
        alertDialog.setMessage(msg);

        // Setting OK Button
        alertDialog.setButton(1, "OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to execute after dialog closed
                Toast.makeText(context, "You clicked on OK", Toast.LENGTH_SHORT).show();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    public static boolean validateName(String txt) {


        return txt.matches( "[A-Z][a-zA-Z]*" );

//        String regx = "^[a-zA-Z\\s]+$";
//        Pattern pattern = Pattern.compile(regx,Pattern.CASE_INSENSITIVE);
//        Matcher matcher = pattern.matcher(txt);
//        return matcher.find();

    }

    public static boolean isValidPassword(final String password) {

        Pattern pattern;
        Matcher matcher;
        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&+=!])(?=\\S+$).{4,}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);

        return matcher.matches();

    }


    public static boolean isValidMail(String email) {
        boolean check;
        Pattern p;
        Matcher m;

        String EMAIL_STRING = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        p = Pattern.compile(EMAIL_STRING);

        m = p.matcher(email);
        check = m.matches();

        if (!check) {
            return false;
        }
        return true;
    }

    public static boolean isValidMobile(String phone) {
        boolean check = false;
        if (!Pattern.matches("[a-zA-Z]+", phone)) {
            if (phone.length() != 10) {
                // if(phone.length() != 10) {
                check = false;
                ;
            } else {
                check = true;
            }
        } else {
            check = false;
        }
        return check;
    }

    public static boolean isValidUserName(String phone) {
        boolean check = false;
        if (!Pattern.matches("[a-zA-Z]+", phone)) {
            if (phone.length() != 10) {
                check = false;
            } else {
                check = true;
            }
        } else {
            check = false;
        }
        if(check==false){
            check = isValidMail(phone);
        }
        return check;
    }

    public ArrayList<SpinValue> getSpinValue(String dropDownType) {
        ArrayList<SpinValue> spinValues = new ArrayList<>();
        spinValues.add(new SpinValue("","Please select"));
        try {
            Gson gson = new Gson();
            SpinResponse spinResponse = gson.fromJson(SharedPreferenceHelper.get(SharedKeys.dropDownList), SpinResponse.class);
            if (dropDownType.equalsIgnoreCase(Constants.religion)) {
                for (int i = 0; i < spinResponse.getReligion().size(); i++) {
                    SpinValue spinValue = new SpinValue("" + spinResponse.getReligion().get(i).getId(),
                            "" + spinResponse.getReligion().get(i).getReligionName());
                    spinValues.add(spinValue);
                }

            } else if (dropDownType.equalsIgnoreCase(Constants.hobby)) {
                for (int i = 0; i < spinResponse.getHobby().size(); i++) {
                    SpinValue spinValue = new SpinValue("" + spinResponse.getHobby().get(i).getPk(),
                            "" + spinResponse.getHobby().get(i).getPk());
                    spinValues.add(spinValue);
                }

            } else if (dropDownType.equalsIgnoreCase(Constants.states)) {
                for (int i = 0; i < spinResponse.getStates().size(); i++) {
                    SpinValue spinValue = new SpinValue("" + spinResponse.getStates().get(i).getId(),
                            "" + spinResponse.getStates().get(i).getStateName());
                    spinValues.add(spinValue);
                }

            }
        }catch (Exception e){}

        return spinValues;

    }


    public ArrayList<SpinValue> getCityOrCast(String dropDownType,String id)  {
        ArrayList<SpinValue> spinValues = new ArrayList<>();
        spinValues.add(new SpinValue("","Please select"));
        try {
            Gson gson = new Gson();
            SpinResponse spinResponse = gson.fromJson(SharedPreferenceHelper.get(SharedKeys.dropDownList), SpinResponse.class);
            if (dropDownType.equalsIgnoreCase(Constants.cities)) {
                for (int i = 0; i < spinResponse.getCities().size(); i++) {
                    if (id.equalsIgnoreCase("" + spinResponse.getCities().get(i).getId())) {
                        SpinValue spinValue = new SpinValue("" + spinResponse.getCities().get(i).getId(),
                                "" + spinResponse.getCities().get(i).getCityName());
                        spinValues.add(spinValue);
                    }
                }

            } else if (dropDownType.equalsIgnoreCase(Constants.casts)) {
                for (int i = 0; i < spinResponse.getCasts().size(); i++) {
                    if (id.equalsIgnoreCase("" + spinResponse.getCasts().get(i).getId())) {
                        SpinValue spinValue = new SpinValue("" + spinResponse.getCasts().get(i).getId(),
                                "" + spinResponse.getCasts().get(i).getCastcatName());
                        spinValues.add(spinValue);
                    }
                }

            }
        }catch (Exception e){}
        return spinValues;

    }



}

