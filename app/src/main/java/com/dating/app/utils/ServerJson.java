package com.dating.app.utils;


import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Bishnu on 5/23/2018.
 */
public class ServerJson {

    private static ServerJson serverJson = null;
    JSONObject jsonObject ;
    private ServerJson(){

    }

    public static ServerJson getInstance(){
        if(serverJson==null)
            serverJson = new ServerJson();

        return serverJson;
    }


    public String jsonSignUP(String id, String fullname, String email,String phone_no,String password){
        try {
            jsonObject = new JSONObject();
            jsonObject.put("id", id);
            jsonObject.put("fullname", fullname);
            jsonObject.put("email", email);
            jsonObject.put("phone_no", phone_no);
            jsonObject.put("password", password);
        }
        catch (Exception e){}
        return jsonObject.toString();
    }


    public String jsonLogin(String userName, String password,String latitude,String longitude){
        try {
            jsonObject = new JSONObject();
            jsonObject.put("username", userName);
            jsonObject.put("password", password);
            jsonObject.put("deviceid", SharedPreferenceHelper.get(SharedKeys.deviceToken));
            jsonObject.put("latitude", latitude);
            jsonObject.put("longitude", longitude);
        }
        catch (Exception e){}

        return jsonObject.toString();
    }

    public String jsonUserList(String latitude,String longitude){
        try {
            jsonObject = new JSONObject();
            jsonObject.put("user_id", SharedPreferenceHelper.get(SharedKeys.userId));
            jsonObject.put("lat", "46.2037010192871");
           // jsonObject.put("lat", latitude);
            jsonObject.put("long", "5.20353984832764");
           // jsonObject.put("long", longitude);
        }
        catch (Exception e){}

        return jsonObject.toString();
    }




    public String jsonForgetPassword(String username, String password, String otp){
        try {
            jsonObject = new JSONObject();
            jsonObject.put("username", username);
            jsonObject.put("password", password);
            jsonObject.put("otp", otp);

        }
        catch (Exception e){}
        return jsonObject.toString();
    }


    public String jsonEmailVerification(String email, String otp){
        try {
            jsonObject = new JSONObject();
            jsonObject.put("email", email);
            jsonObject.put("otp", otp);

        }
        catch (Exception e){}
        return jsonObject.toString();
    }

    public String jsonOTPEmail(String email){
        try {
            jsonObject = new JSONObject();
            jsonObject.put("email", email);

        }
        catch (Exception e){}
        return jsonObject.toString();
    }


    public String jsonOTPPhone(String phone_no){
        try {
            jsonObject = new JSONObject();
            jsonObject.put("phone_no", phone_no);

        }
        catch (Exception e){}
        return jsonObject.toString();
    }

    public String otpLoginJson(String username){
        try {
            jsonObject = new JSONObject();
            jsonObject.put("username", username);
        }
        catch (Exception e){}

        return jsonObject.toString();
    }



//like JSON Server
    public String jsonLike(String id, String issupperlike){
        try {
            jsonObject = new JSONObject();
            jsonObject.put("liked", id);
            jsonObject.put("liked_by", SharedPreferenceHelper.get(SharedKeys.userId));
            jsonObject.put("is_superlike", issupperlike);
        }
        catch (Exception e){}

        return jsonObject.toString();
    }

    public String jsonWishList(String id){
        try {
            jsonObject = new JSONObject();
            jsonObject.put("userwish", id);
        }
        catch (Exception e){}

        return jsonObject.toString();
    }

    //Unmatch JSON Server

    public String jsonUnmatch(String is_match, String is_block){
        try {
            jsonObject = new JSONObject();
            jsonObject.put("is_match", is_match);
            jsonObject.put("is_block", is_block);

        }
        catch (Exception e){}

        return jsonObject.toString();
    }
  //getMyplan

    /*public String otpMyplan(String is_match, String is_block){
        try {
            jsonObject.put("is_match", is_match);
            jsonObject.put("is_block", is_block);

        }
        catch (Exception e){}

        return jsonObject.toString();
    }*/




    public String jsonProfileEditing(String religion, String about_you,String current_address, String permanent_address,String pin_code, String city,String state,String weight,String height,String body_type,String marital_status,String  income,String gender){
        try {
            jsonObject = new JSONObject();
            jsonObject.put("religion", religion);
            jsonObject.put("about_you",  SharedPreferenceHelper.get(SharedKeys.userId));
            jsonObject.put("current_address", current_address);
            jsonObject.put("permanent_address", permanent_address);
            jsonObject.put("pin_code", pin_code);
            jsonObject.put("city", city);
            jsonObject.put("state", state);
            jsonObject.put("weight", weight);
            jsonObject.put("height", height);
            jsonObject.put("body_type", body_type);
            jsonObject.put("marital_status", marital_status);
            jsonObject.put("income", income);
            jsonObject.put("gender", gender);
        }
        catch (Exception e){}

        return jsonObject.toString();
    }


    public String jsonCheckSum(String email,String amount){
        try {
            jsonObject = new JSONObject();
            jsonObject.put("email", email);
            jsonObject.put("bill_amount", amount);
        }
        catch (Exception e){}

        return jsonObject.toString();
    }

    public String planPurchase(String boostId,String planId,String isPaid,String superId,String price){
        try {
            jsonObject = new JSONObject();
            jsonObject.put("boost_id", boostId);
            jsonObject.put("plan_id", planId);
            jsonObject.put("is_paid", isPaid);
            jsonObject.put("superlike_id", superId);
            jsonObject.put("price", price);
        }
        catch (Exception e){}

        return jsonObject.toString();
    }

}
