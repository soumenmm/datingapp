package com.dating.app.utils;

public class Constants {

    public static final String AppID = "4be21bc8e84e4f5b890a46b3593a580b";
    // DropDownType
    public static final String casts = "casts";
    public static final String cities = "cities";
    public static final String religion = "religion";
    public static final String hobby = "hobby";
    public static final String states = "states";

    //public static final String rootURL = "http://50.62.57.4:8000/";
    public static final String rootURL = "http://18.222.106.171/";
    public static final String signUP = rootURL+"signup";
    public static final String login = rootURL+"login";
    public static final String signUpVerify = rootURL+"phoneVerification";
    public static final String emailVerification = rootURL+"emailVerification";
    public static final String otpForEmail = rootURL+"otpforemail";
    public static final String otpForPhone = rootURL+"otpforphone";
    public static final String forgotPassword = rootURL+"update-password";
    public static final String uploadImages = rootURL+"upload-images";
    public static final String replaceImage = rootURL+"replaceimage";
    public static final String settingGet = rootURL+"settings-get";
    public static final String userProfileSetting = rootURL+"profile";
    public static final String dropDownList = rootURL+"dropdownlist";
    public static final String sendRequest = rootURL+"getlikedbyme";
    public static final String receivedRequest = rootURL+"getlikedtome";
    public static final String dropUnmatch = rootURL+"unmatch";
    public static final String userListing = rootURL+"userlisting";
    public static final String planList = rootURL+"planlist";
    public static final String like = rootURL+"like";
    public static final String addToWishList = rootURL+"addtowishlist";
    public static final String profileSetting = rootURL+"profilesettings";
    public static final String makeDefaultProfilePic = rootURL+"makedefaultprofilepic";
    public static final String profile = rootURL+"profile";
    public static final String profileEditing = rootURL+"profile-editing";
    public static final String getMyplan = rootURL+"getmyplan";
    public static final String myWishList = rootURL+"mywishlist";
    public static final String myFriends = rootURL+"usermatches";
    public static final String genratechecksum = rootURL+"genratechecksum/";
    public static final String getWalletbalance = rootURL+"walletbalance";
    public static final String planPurchase = rootURL+"enrollusersubscription/";






}
