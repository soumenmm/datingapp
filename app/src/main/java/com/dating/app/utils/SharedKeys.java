package com.dating.app.utils;

public class SharedKeys {
    public static String myFriends="myFriends";
    public static String sendRequest="sendRequest";
    public static String receivedRequest="receivedRequest";
    public static String deviceToken="deviceToken";
    public static String userName="userName";
    public static String password="password";
    public static String userId="userId";
    public static String profileImage="profileImage";
    public static String userData="userData";
    public static String name="name";
    public static String email="email";
    public static String mobileNo="mobileNo";
    public static String dropDownList="dropDownList";
}
