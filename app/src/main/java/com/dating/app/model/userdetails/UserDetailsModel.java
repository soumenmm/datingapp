package com.dating.app.model.userdetails;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.view.ViewPager;

import com.dating.app.adapter.ImagePager;
import com.dating.app.model.userlist.DataItem;

import java.io.Serializable;
import java.util.ArrayList;

public class UserDetailsModel implements Serializable {
    private String strAbout;
    private String strEmail;
    private String strAge;
    private String strLocation;
    private String strPhoneVerification;
    private String strBodyType;
    private String strMaritalStatus;
    private String strGender;
    private String editEmailVerification;
    private String strCurrentAddress;
    private String strPermanentAddress;
    private String strEducation;
    private String strPincode;
    private String strWeight;
    private String strheight;
    private String imageUrl1;
    private String imageUrl2;
    private String imageUrl3;
    private String imageUrl4;
    private String imageUrl5;

    public int getPicCount() {
        return picCount;
    }

    public void setPicCount(int picCount) {
        this.picCount = picCount;
    }

    private int picCount;

    public String getImageUrl1() {
        return imageUrl1;
    }

    public void setImageUrl1(String imageUrl1) {
        this.imageUrl1 = imageUrl1;
    }

    public String getImageUrl2() {
        return imageUrl2;
    }

    public void setImageUrl2(String imageUrl2) {
        this.imageUrl2 = imageUrl2;
    }

    public String getImageUrl3() {
        return imageUrl3;
    }

    public void setImageUrl3(String imageUrl3) {
        this.imageUrl3 = imageUrl3;
    }

    public String getImageUrl4() {
        return imageUrl4;
    }

    public void setImageUrl4(String imageUrl4) {
        this.imageUrl4 = imageUrl4;
    }

    public String getImageUrl5() {
        return imageUrl5;
    }

    public void setImageUrl5(String imageUrl5) {
        this.imageUrl5 = imageUrl5;
    }

    public String getImageUrl6() {
        return imageUrl6;
    }

    public void setImageUrl6(String imageUrl6) {
        this.imageUrl6 = imageUrl6;
    }

    private String imageUrl6;

    public String getStrAbout() {
        return strAbout;
    }

    public void setStrAbout(String strAbout) {
        this.strAbout = strAbout;
    }

    public String getStrEmail() {
        return strEmail;
    }

    public void setStrEmail(String strEmail) {
        this.strEmail = strEmail;
    }

    public String getStrAge() {
        return strAge;
    }

    public void setStrAge(String strAge) {
        this.strAge = strAge;
    }

    public String getStrLocation() {
        return strLocation;
    }

    public void setStrLocation(String strLocation) {
        this.strLocation = strLocation;
    }

    public String getStrPhoneVerification() {
        return strPhoneVerification;
    }

    public void setStrPhoneVerification(String strPhoneVerification) {
        this.strPhoneVerification = strPhoneVerification;
    }

    public String getStrBodyType() {
        return strBodyType;
    }

    public void setStrBodyType(String strBodyType) {
        this.strBodyType = strBodyType;
    }

    public String getStrMaritalStatus() {
        return strMaritalStatus;
    }

    public void setStrMaritalStatus(String strMaritalStatus) {
        this.strMaritalStatus = strMaritalStatus;
    }

    public String getStrGender() {
        return strGender;
    }

    public void setStrGender(String strGender) {
        this.strGender = strGender;
    }

    public String getEditEmailVerification() {
        return editEmailVerification;
    }

    public void setEditEmailVerification(String editEmailVerification) {
        this.editEmailVerification = editEmailVerification;
    }

    public String getStrCurrentAddress() {
        return strCurrentAddress;
    }

    public void setStrCurrentAddress(String strCurrentAddress) {
        this.strCurrentAddress = strCurrentAddress;
    }

    public String getStrPermanentAddress() {
        return strPermanentAddress;
    }

    public void setStrPermanentAddress(String strPermanentAddress) {
        this.strPermanentAddress = strPermanentAddress;
    }

    public String getStrEducation() {
        return strEducation;
    }

    public void setStrEducation(String strEducation) {
        this.strEducation = strEducation;
    }

    public String getStrPincode() {
        return strPincode;
    }

    public void setStrPincode(String strPincode) {
        this.strPincode = strPincode;
    }

    public String getStrWeight() {
        return strWeight;
    }

    public void setStrWeight(String strWeight) {
        this.strWeight = strWeight;
    }

    public String getStrheight() {
        return strheight;
    }

    public void setStrheight(String strheight) {
        this.strheight = strheight;
    }

    public String getStrIncome() {
        return strIncome;
    }

    public void setStrIncome(String strIncome) {
        this.strIncome = strIncome;
    }

    private String strIncome;
}
