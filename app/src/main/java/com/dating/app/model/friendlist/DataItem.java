package com.dating.app.model.friendlist;

import com.google.gson.annotations.SerializedName;

public class DataItem{

	@SerializedName("is_match")
	private boolean isMatch;

	@SerializedName("is_superlike")
	private boolean isSuperlike;

	@SerializedName("is_block")
	private boolean isBlock;

	@SerializedName("liked_at")
	private String likedAt;

	@SerializedName("matched_at")
	private String matchedAt;

	@SerializedName("liked_by")
	private Liked liked;

	public void setIsMatch(boolean isMatch){
		this.isMatch = isMatch;
	}

	public boolean isIsMatch(){
		return isMatch;
	}

	public void setIsSuperlike(boolean isSuperlike){
		this.isSuperlike = isSuperlike;
	}

	public boolean isIsSuperlike(){
		return isSuperlike;
	}

	public void setIsBlock(boolean isBlock){
		this.isBlock = isBlock;
	}

	public boolean isIsBlock(){
		return isBlock;
	}

	public void setLikedAt(String likedAt){
		this.likedAt = likedAt;
	}

	public String getLikedAt(){
		return likedAt;
	}

	public void setMatchedAt(String matchedAt){
		this.matchedAt = matchedAt;
	}

	public String getMatchedAt(){
		return matchedAt;
	}

	public void setLiked(Liked liked){
		this.liked = liked;
	}

	public Liked getLiked(){
		return liked;
	}
}