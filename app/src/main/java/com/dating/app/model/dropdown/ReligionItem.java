package com.dating.app.model.dropdown;

import com.google.gson.annotations.SerializedName;

public class ReligionItem{

	@SerializedName("id")
	private int id;

	@SerializedName("religion_name")
	private String religionName;

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setReligionName(String religionName){
		this.religionName = religionName;
	}

	public String getReligionName(){
		return religionName;
	}
}