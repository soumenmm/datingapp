package com.dating.app.model.dropdown;

import com.google.gson.annotations.SerializedName;

public class HobbyItem{

	@SerializedName("pk")
	private int pk;

	public void setPk(int pk){
		this.pk = pk;
	}

	public int getPk(){
		return pk;
	}
}