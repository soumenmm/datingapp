package com.dating.app.model.plan;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PlanResponse {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("plandetail")
    @Expose
    private List<Plandetail> plandetail = null;
    @SerializedName("aboutplan")
    @Expose
    private String aboutplan;
    @SerializedName("is_active")
    @Expose
    private Boolean isActive;
    @SerializedName("created_at")
    @Expose
    private String createdAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Plandetail> getPlandetail() {
        return plandetail;
    }

    public void setPlandetail(List<Plandetail> plandetail) {
        this.plandetail = plandetail;
    }

    public String getAboutplan() {
        return aboutplan;
    }

    public void setAboutplan(String aboutplan) {
        this.aboutplan = aboutplan;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

}