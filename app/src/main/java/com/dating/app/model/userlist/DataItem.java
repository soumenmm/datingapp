package com.dating.app.model.userlist;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataItem implements Serializable {
	@SerializedName("id")
	@Expose
	private Integer id;
	@SerializedName("fullname")
	@Expose
	private String fullname;
	@SerializedName("email")
	@Expose
	private String email;
	@SerializedName("phone_no")
	@Expose
	private String phoneNo;
	@SerializedName("age")
	@Expose
	private Integer age;
	@SerializedName("profileimages")
	@Expose
	private List<Userprofileimage> userprofileimages = null;
	@SerializedName("state")
	@Expose
	private String state;
	@SerializedName("city")
	@Expose
	private String city;
	@SerializedName("cast")
	@Expose
	private Integer cast;
	@SerializedName("religion")
	@Expose
	private Object religion;
	@SerializedName("current_address")
	@Expose
	private String currentAddress;
	@SerializedName("permanent_address")
	@Expose
	private String permanentAddress;
	@SerializedName("pin_code")
	@Expose
	private String pinCode;
	@SerializedName("phone_verification")
	@Expose
	private Boolean phoneVerification;
	@SerializedName("weight")
	@Expose
	private String weight;
	@SerializedName("height")
	@Expose
	private String height;
	@SerializedName("body_type")
	@Expose
	private Integer bodyType;
	@SerializedName("marital_status")
	@Expose
	private String maritalStatus;
	@SerializedName("income")
	@Expose
	private String income;
	@SerializedName("gender")
	@Expose
	private Object gender;
	@SerializedName("email_verification")
	@Expose
	private Boolean emailVerification;
	@SerializedName("about_you")
	@Expose
	private String aboutYou;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public List<Userprofileimage> getUserprofileimages() {
		return userprofileimages;
	}

	public void setUserprofileimages(List<Userprofileimage> userprofileimages) {
		this.userprofileimages = userprofileimages;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Integer getCast() {
		return cast;
	}

	public void setCast(Integer cast) {
		this.cast = cast;
	}

	public Object getReligion() {
		return religion;
	}

	public void setReligion(Object religion) {
		this.religion = religion;
	}

	public String getCurrentAddress() {
		return currentAddress;
	}

	public void setCurrentAddress(String currentAddress) {
		this.currentAddress = currentAddress;
	}

	public String getPermanentAddress() {
		return permanentAddress;
	}

	public void setPermanentAddress(String permanentAddress) {
		this.permanentAddress = permanentAddress;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public Boolean getPhoneVerification() {
		return phoneVerification;
	}

	public void setPhoneVerification(Boolean phoneVerification) {
		this.phoneVerification = phoneVerification;
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	public String getHeight() {
		return height;
	}

	public void setHeight(String height) {
		this.height = height;
	}

	public Integer getBodyType() {
		return bodyType;
	}

	public void setBodyType(Integer bodyType) {
		this.bodyType = bodyType;
	}

	public String getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public String getIncome() {
		return income;
	}

	public void setIncome(String income) {
		this.income = income;
	}

	public Object getGender() {
		return gender;
	}

	public void setGender(Object gender) {
		this.gender = gender;
	}

	public Boolean getEmailVerification() {
		return emailVerification;
	}

	public void setEmailVerification(Boolean emailVerification) {
		this.emailVerification = emailVerification;
	}

	public String getAboutYou() {
		return aboutYou;
	}

	public void setAboutYou(String aboutYou) {
		this.aboutYou = aboutYou;
	}

}