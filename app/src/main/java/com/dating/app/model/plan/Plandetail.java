package com.dating.app.model.plan;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Plandetail implements Serializable {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("validity")
    @Expose
    private Integer validity;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("superlike")
    @Expose
    private Integer superlike;
    @SerializedName("video_enabled")
    @Expose
    private Boolean videoEnabled;
    @SerializedName("audio_enabled")
    @Expose
    private Boolean audioEnabled;
    @SerializedName("boost")
    @Expose
    private Integer boost;
    @SerializedName("unlimited_like")
    @Expose
    private Boolean unlimitedLike;
    @SerializedName("sees_control")
    @Expose
    private Boolean seesControl;
    @SerializedName("hide_ads")
    @Expose
    private Boolean hideAds;
    @SerializedName("profile_control")
    @Expose
    private Boolean profileControl;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public Integer getValidity() {
        return validity;
    }

    public void setValidity(Integer validity) {
        this.validity = validity;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getSuperlike() {
        return superlike;
    }

    public void setSuperlike(Integer superlike) {
        this.superlike = superlike;
    }

    public Boolean getVideoEnabled() {
        return videoEnabled;
    }

    public void setVideoEnabled(Boolean videoEnabled) {
        this.videoEnabled = videoEnabled;
    }

    public Boolean getAudioEnabled() {
        return audioEnabled;
    }

    public void setAudioEnabled(Boolean audioEnabled) {
        this.audioEnabled = audioEnabled;
    }

    public Integer getBoost() {
        return boost;
    }

    public void setBoost(Integer boost) {
        this.boost = boost;
    }

    public Boolean getUnlimitedLike() {
        return unlimitedLike;
    }

    public void setUnlimitedLike(Boolean unlimitedLike) {
        this.unlimitedLike = unlimitedLike;
    }

    public Boolean getSeesControl() {
        return seesControl;
    }

    public void setSeesControl(Boolean seesControl) {
        this.seesControl = seesControl;
    }

    public Boolean getHideAds() {
        return hideAds;
    }

    public void setHideAds(Boolean hideAds) {
        this.hideAds = hideAds;
    }

    public Boolean getProfileControl() {
        return profileControl;
    }

    public void setProfileControl(Boolean profileControl) {
        this.profileControl = profileControl;
    }

}
