package com.dating.app.model.dropdown;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class SpinResponse{

	@SerializedName("casts")
	private List<CastsItem> casts;

	@SerializedName("cities")
	private List<CitiesItem> cities;

	@SerializedName("message")
	private String message;

	@SerializedName("religion")
	private List<ReligionItem> religion;

	@SerializedName("hobby")
	private List<HobbyItem> hobby;

	@SerializedName("status")
	private String status;

	@SerializedName("states")
	private List<StatesItem> states;

	public void setCasts(List<CastsItem> casts){
		this.casts = casts;
	}

	public List<CastsItem> getCasts(){
		return casts;
	}

	public void setCities(List<CitiesItem> cities){
		this.cities = cities;
	}

	public List<CitiesItem> getCities(){
		return cities;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setReligion(List<ReligionItem> religion){
		this.religion = religion;
	}

	public List<ReligionItem> getReligion(){
		return religion;
	}

	public void setHobby(List<HobbyItem> hobby){
		this.hobby = hobby;
	}

	public List<HobbyItem> getHobby(){
		return hobby;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	public void setStates(List<StatesItem> states){
		this.states = states;
	}

	public List<StatesItem> getStates(){
		return states;
	}
}