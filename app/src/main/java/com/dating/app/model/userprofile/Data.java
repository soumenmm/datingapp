package com.dating.app.model.userprofile;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Data{

	@SerializedName("income")
	private Object income;

	@SerializedName("phone_no")
	private String phoneNo;

	@SerializedName("gender")
	private Object gender;

	@SerializedName("city")
	private Object city;

	@SerializedName("email_verification")
	private boolean emailVerification;

	@SerializedName("about_you")
	private Object aboutYou;

	@SerializedName("pin_code")
	private Object pinCode;

	@SerializedName("weight")
	private Object weight;

	@SerializedName("permanent_address")
	private Object permanentAddress;

	@SerializedName("phone_verification")
	private boolean phoneVerification;

	@SerializedName("profileimages")
	private List<UserprofileimagesItem> userprofileimages;

	@SerializedName("religion")
	private Object religion;

	@SerializedName("current_address")
	private Object currentAddress;

	@SerializedName("cast")
	private Object cast;

	@SerializedName("marital_status")
	private Object maritalStatus;

	@SerializedName("body_type")
	private Object bodyType;

	@SerializedName("id")
	private int id;

	@SerializedName("fullname")
	private String fullname;

	@SerializedName("state")
	private Object state;

	@SerializedName("email")
	private String email;

	@SerializedName("age")
	private int age;

	@SerializedName("height")
	private Object height;

	public void setIncome(Object income){
		this.income = income;
	}

	public Object getIncome(){
		return income;
	}

	public void setPhoneNo(String phoneNo){
		this.phoneNo = phoneNo;
	}

	public String getPhoneNo(){
		return phoneNo;
	}

	public void setGender(Object gender){
		this.gender = gender;
	}

	public Object getGender(){
		return gender;
	}

	public void setCity(Object city){
		this.city = city;
	}

	public Object getCity(){
		return city;
	}

	public void setEmailVerification(boolean emailVerification){
		this.emailVerification = emailVerification;
	}

	public boolean isEmailVerification(){
		return emailVerification;
	}

	public void setAboutYou(Object aboutYou){
		this.aboutYou = aboutYou;
	}

	public Object getAboutYou(){
		return aboutYou;
	}

	public void setPinCode(Object pinCode){
		this.pinCode = pinCode;
	}

	public Object getPinCode(){
		return pinCode;
	}

	public void setWeight(Object weight){
		this.weight = weight;
	}

	public Object getWeight(){
		return weight;
	}

	public void setPermanentAddress(Object permanentAddress){
		this.permanentAddress = permanentAddress;
	}

	public Object getPermanentAddress(){
		return permanentAddress;
	}

	public void setPhoneVerification(boolean phoneVerification){
		this.phoneVerification = phoneVerification;
	}

	public boolean isPhoneVerification(){
		return phoneVerification;
	}

	public void setUserprofileimages(List<UserprofileimagesItem> userprofileimages){
		this.userprofileimages = userprofileimages;
	}

	public List<UserprofileimagesItem> getUserprofileimages(){
		return userprofileimages;
	}

	public void setReligion(Object religion){
		this.religion = religion;
	}

	public Object getReligion(){
		return religion;
	}

	public void setCurrentAddress(Object currentAddress){
		this.currentAddress = currentAddress;
	}

	public Object getCurrentAddress(){
		return currentAddress;
	}

	public void setCast(Object cast){
		this.cast = cast;
	}

	public Object getCast(){
		return cast;
	}

	public void setMaritalStatus(Object maritalStatus){
		this.maritalStatus = maritalStatus;
	}

	public Object getMaritalStatus(){
		return maritalStatus;
	}

	public void setBodyType(Object bodyType){
		this.bodyType = bodyType;
	}

	public Object getBodyType(){
		return bodyType;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setFullname(String fullname){
		this.fullname = fullname;
	}

	public String getFullname(){
		return fullname;
	}

	public void setState(Object state){
		this.state = state;
	}

	public Object getState(){
		return state;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	public void setAge(int age){
		this.age = age;
	}

	public int getAge(){
		return age;
	}

	public void setHeight(Object height){
		this.height = height;
	}

	public Object getHeight(){
		return height;
	}
}