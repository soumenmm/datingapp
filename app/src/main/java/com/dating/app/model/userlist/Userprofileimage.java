package com.dating.app.model.userlist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Userprofileimage implements Serializable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("profilepics")
    @Expose
    private String userProfilePics;
    @SerializedName("is_profile")
    @Expose
    private Boolean isProfile;
    @SerializedName("user_id")
    @Expose
    private Integer userId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserProfilePics() {
        return userProfilePics;
    }

    public void setUserProfilePics(String userProfilePics) {
        this.userProfilePics = userProfilePics;
    }

    public Boolean getIsProfile() {
        return isProfile;
    }

    public void setIsProfile(Boolean isProfile) {
        this.isProfile = isProfile;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

}
