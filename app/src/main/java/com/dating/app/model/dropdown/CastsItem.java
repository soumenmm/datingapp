package com.dating.app.model.dropdown;

import com.google.gson.annotations.SerializedName;

public class CastsItem{

	@SerializedName("religion_id")
	private int religionId;

	@SerializedName("castcat_name")
	private String castcatName;

	@SerializedName("id")
	private int id;

	public void setReligionId(int religionId){
		this.religionId = religionId;
	}

	public int getReligionId(){
		return religionId;
	}

	public void setCastcatName(String castcatName){
		this.castcatName = castcatName;
	}

	public String getCastcatName(){
		return castcatName;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}
}