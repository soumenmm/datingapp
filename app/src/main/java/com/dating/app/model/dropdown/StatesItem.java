package com.dating.app.model.dropdown;

import com.google.gson.annotations.SerializedName;

public class StatesItem{

	@SerializedName("state_name")
	private String stateName;

	@SerializedName("id")
	private int id;

	public void setStateName(String stateName){
		this.stateName = stateName;
	}

	public String getStateName(){
		return stateName;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}
}