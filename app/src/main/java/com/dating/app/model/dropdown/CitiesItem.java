package com.dating.app.model.dropdown;

import com.google.gson.annotations.SerializedName;

public class CitiesItem{

	@SerializedName("city_name")
	private String cityName;

	@SerializedName("id")
	private int id;

	@SerializedName("state_id")
	private int stateId;

	public void setCityName(String cityName){
		this.cityName = cityName;
	}

	public String getCityName(){
		return cityName;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setStateId(int stateId){
		this.stateId = stateId;
	}

	public int getStateId(){
		return stateId;
	}
}