package com.dating.app.model.userprofile;

import com.google.gson.annotations.SerializedName;

public class UserprofileimagesItem{

	@SerializedName("user_id")
	private int userId;

	@SerializedName("id")
	private int id;

	@SerializedName("profilepics")
	private String userProfilePics;

	@SerializedName("is_profile")
	private boolean isProfile;

	public void setUserId(int userId){
		this.userId = userId;
	}

	public int getUserId(){
		return userId;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setUserProfilePics(String userProfilePics){
		this.userProfilePics = userProfilePics;
	}

	public String getUserProfilePics(){
		return userProfilePics;
	}

	public void setIsProfile(boolean isProfile){
		this.isProfile = isProfile;
	}

	public boolean isIsProfile(){
		return isProfile;
	}
}