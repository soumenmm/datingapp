package com.dating.app.model.login;

import com.google.gson.annotations.SerializedName;

public class UserprofileimagesItem{

	@SerializedName("id")
	private int id;

	@SerializedName("profilepics")
	private String userProfilePics;

	@SerializedName("is_profile")
	private boolean isProfile;

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setUserProfilePics(String userProfilePics){
		this.userProfilePics = userProfilePics;
	}

	public String getUserProfilePics(){
		return userProfilePics;
	}

	public void setIsProfile(boolean isProfile){
		this.isProfile = isProfile;
	}

	public boolean isIsProfile(){
		return isProfile;
	}
}