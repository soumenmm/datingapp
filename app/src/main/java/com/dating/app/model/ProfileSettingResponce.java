package com.dating.app.model;

import java.util.List;

import com.google.gson.annotations.SerializedName;


public class ProfileSettingResponce{

	@SerializedName("to_age")
	private String toAge;

	@SerializedName("gender")
	private String gender;

	@SerializedName("from_age")
	private String fromAge;

	@SerializedName("hobies")
	private List<Integer> hobies;

	@SerializedName("religion")
	private int religion;

	public void setToAge(String toAge){
		this.toAge = toAge;
	}

	public String getToAge(){
		return toAge;
	}

	public void setGender(String gender){
		this.gender = gender;
	}

	public String getGender(){
		return gender;
	}

	public void setFromAge(String fromAge){
		this.fromAge = fromAge;
	}

	public String getFromAge(){
		return fromAge;
	}

	public void setHobies(List<Integer> hobies){
		this.hobies = hobies;
	}

	public List<Integer> getHobies(){
		return hobies;
	}

	public void setReligion(int religion){
		this.religion = religion;
	}

	public int getReligion(){
		return religion;
	}
}