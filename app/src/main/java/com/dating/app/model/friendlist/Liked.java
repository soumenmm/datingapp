package com.dating.app.model.friendlist;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Liked{

	@SerializedName("income")
	private String income;

	@SerializedName("phone_no")
	private String phoneNo;

	@SerializedName("gender")
	private String gender;

	@SerializedName("city")
	private String city;

	@SerializedName("email_verification")
	private boolean emailVerification;

	@SerializedName("about_you")
	private String aboutYou;

	@SerializedName("pin_code")
	private String pinCode;

	@SerializedName("weight")
	private String weight;

	@SerializedName("permanent_address")
	private String permanentAddress;

	@SerializedName("phone_verification")
	private boolean phoneVerification;

	@SerializedName("profileimages")
	private List<UserprofileimagesItem> userprofileimages;

	@SerializedName("religion")
	private Object religion;

	@SerializedName("current_address")
	private String currentAddress;

	@SerializedName("cast")
	private int cast;

	@SerializedName("marital_status")
	private String maritalStatus;

	@SerializedName("body_type")
	private int bodyType;

	@SerializedName("id")
	private int id;

	@SerializedName("fullname")
	private String fullname;

	@SerializedName("state")
	private String state;

	@SerializedName("email")
	private String email;

	@SerializedName("age")
	private int age;

	@SerializedName("height")
	private String height;

	public void setIncome(String income){
		this.income = income;
	}

	public String getIncome(){
		return income;
	}

	public void setPhoneNo(String phoneNo){
		this.phoneNo = phoneNo;
	}

	public String getPhoneNo(){
		return phoneNo;
	}

	public void setGender(String gender){
		this.gender = gender;
	}

	public String getGender(){
		return gender;
	}

	public void setCity(String city){
		this.city = city;
	}

	public String getCity(){
		return city;
	}

	public void setEmailVerification(boolean emailVerification){
		this.emailVerification = emailVerification;
	}

	public boolean isEmailVerification(){
		return emailVerification;
	}

	public void setAboutYou(String aboutYou){
		this.aboutYou = aboutYou;
	}

	public String getAboutYou(){
		return aboutYou;
	}

	public void setPinCode(String pinCode){
		this.pinCode = pinCode;
	}

	public String getPinCode(){
		return pinCode;
	}

	public void setWeight(String weight){
		this.weight = weight;
	}

	public String getWeight(){
		return weight;
	}

	public void setPermanentAddress(String permanentAddress){
		this.permanentAddress = permanentAddress;
	}

	public String getPermanentAddress(){
		return permanentAddress;
	}

	public void setPhoneVerification(boolean phoneVerification){
		this.phoneVerification = phoneVerification;
	}

	public boolean isPhoneVerification(){
		return phoneVerification;
	}

	public void setUserprofileimages(List<UserprofileimagesItem> userprofileimages){
		this.userprofileimages = userprofileimages;
	}

	public List<UserprofileimagesItem> getUserprofileimages(){
		return userprofileimages;
	}

	public void setReligion(Object religion){
		this.religion = religion;
	}

	public Object getReligion(){
		return religion;
	}

	public void setCurrentAddress(String currentAddress){
		this.currentAddress = currentAddress;
	}

	public String getCurrentAddress(){
		return currentAddress;
	}

	public void setCast(int cast){
		this.cast = cast;
	}

	public int getCast(){
		return cast;
	}

	public void setMaritalStatus(String maritalStatus){
		this.maritalStatus = maritalStatus;
	}

	public String getMaritalStatus(){
		return maritalStatus;
	}

	public void setBodyType(int bodyType){
		this.bodyType = bodyType;
	}

	public int getBodyType(){
		return bodyType;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setFullname(String fullname){
		this.fullname = fullname;
	}

	public String getFullname(){
		return fullname;
	}

	public void setState(String state){
		this.state = state;
	}

	public String getState(){
		return state;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	public void setAge(int age){
		this.age = age;
	}

	public int getAge(){
		return age;
	}

	public void setHeight(String height){
		this.height = height;
	}

	public String getHeight(){
		return height;
	}
}