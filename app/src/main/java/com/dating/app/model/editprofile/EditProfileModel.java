package com.dating.app.model.editprofile;

public class EditProfileModel{
	private String income;
	private String education;
	private String gender;
	private String city;
	private String aboutYou;
	private String pinCode;
	private String weight;
	private String permanentAddress;
	private String religion;
	private String currentAddress;
	private String cast;
	private String maritalStatus;
	private String bodyType;
	private String state;
	private String height;

	public void setIncome(String income){
		this.income = income;
	}

	public String getIncome(){
		return income;
	}

	public void setEducation(String education){
		this.education = education;
	}

	public String getEducation(){
		return education;
	}

	public void setGender(String gender){
		this.gender = gender;
	}

	public String getGender(){
		return gender;
	}

	public void setCity(String city){
		this.city = city;
	}

	public String getCity(){
		return city;
	}

	public void setAboutYou(String aboutYou){
		this.aboutYou = aboutYou;
	}

	public String getAboutYou(){
		return aboutYou;
	}

	public void setPinCode(String pinCode){
		this.pinCode = pinCode;
	}

	public String getPinCode(){
		return pinCode;
	}

	public void setWeight(String weight){
		this.weight = weight;
	}

	public String getWeight(){
		return weight;
	}

	public void setPermanentAddress(String permanentAddress){
		this.permanentAddress = permanentAddress;
	}

	public String getPermanentAddress(){
		return permanentAddress;
	}

	public void setReligion(String religion){
		this.religion = religion;
	}

	public String getReligion(){
		return religion;
	}

	public void setCurrentAddress(String currentAddress){
		this.currentAddress = currentAddress;
	}

	public String getCurrentAddress(){
		return currentAddress;
	}

	public void setCast(String cast){
		this.cast = cast;
	}

	public String getCast(){
		return cast;
	}

	public void setMaritalStatus(String maritalStatus){
		this.maritalStatus = maritalStatus;
	}

	public String getMaritalStatus(){
		return maritalStatus;
	}

	public void setBodyType(String bodyType){
		this.bodyType = bodyType;
	}

	public String getBodyType(){
		return bodyType;
	}

	public void setState(String state){
		this.state = state;
	}

	public String getState(){
		return state;
	}

	public void setHeight(String height){
		this.height = height;
	}

	public String getHeight(){
		return height;
	}
}
