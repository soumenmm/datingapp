package com.dating.app.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TextView;

import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.interfaces.OnSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;
import com.crystal.crystalrangeseekbar.widgets.CrystalSeekbar;
import com.dating.app.R;

import info.hoang8f.android.segmented.SegmentedGroup;

public class SettingActivity extends AppCompatActivity implements  CompoundButton.OnCheckedChangeListener{

    private Toolbar toolbar;
    private SegmentedGroup segmentShowMe;
    private SegmentedGroup segmentDistance;
    private CrystalRangeSeekbar rangeSeekBar;
    private TextView txtMin;
    private TextView txtMax;
    private RadioButton radioMale,radioFemale,radioBoth,radio_Km,radio_Meters,radio_Mile;
    private SwitchCompat swicthShowMeonApp,swictchShareFeed,swictchShareContent,swictchSharePhoto,swictchShareBio,swictchShareSchool,swictchShareWork;
    private SwitchCompat swictchNewMatches,swictchMessages,swictchShareLike,swictchSharesupperlike,swictchNewMessages,swictchSharePromotion;
    private CrystalSeekbar distanceRange;
    private TextView txtDistance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_newsetting);

        initUI();
    }

    void initUI(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        segmentShowMe = (SegmentedGroup) findViewById(R.id.segmentShowMe);
        segmentDistance = (SegmentedGroup) findViewById(R.id.segmentDistance);
        rangeSeekBar = (CrystalRangeSeekbar) findViewById(R.id.rangeSeekBar);
        txtMin = (TextView) findViewById(R.id.txtMin);
        txtMax = (TextView) findViewById(R.id.txtMax);
        radioMale = (RadioButton) findViewById(R.id.radioMale);
        radioFemale = (RadioButton) findViewById(R.id.radioFemale);
        radioBoth = (RadioButton) findViewById(R.id.radioBoth);
        radio_Km = (RadioButton) findViewById(R.id.radio_Km);
        radio_Meters = (RadioButton) findViewById(R.id.radio_Meters);
        radio_Mile = (RadioButton) findViewById(R.id.radio_Mile);

        swicthShowMeonApp = (SwitchCompat) findViewById(R.id.swicthShowMeonApp);
        swictchShareFeed = (SwitchCompat) findViewById(R.id.swictchShareFeed);
        swictchShareContent = (SwitchCompat) findViewById(R.id.swictchShareContent);
        swictchSharePhoto = (SwitchCompat) findViewById(R.id.swictchSharePhoto);
        swictchShareBio = (SwitchCompat) findViewById(R.id.swictchShareBio);
        swictchShareSchool = (SwitchCompat) findViewById(R.id.swictchShareSchool);
        swictchShareWork = (SwitchCompat) findViewById(R.id.swictchShareWork);

        swictchNewMatches = (SwitchCompat) findViewById(R.id.swictchNewMatches);
        swictchMessages = (SwitchCompat) findViewById(R.id.swictchMessages);
        swictchSharesupperlike = (SwitchCompat) findViewById(R.id.swictchSharesupperlike);
        swictchNewMessages = (SwitchCompat) findViewById(R.id.swictchNewMessages);
        swictchShareLike = (SwitchCompat) findViewById(R.id.swictchShareLike);
        swictchSharePromotion = (SwitchCompat) findViewById(R.id.swictchSharePromotion);

        rangeSeekBar = (CrystalRangeSeekbar) findViewById(R.id.rangeSeekBar);
        distanceRange = (CrystalSeekbar) findViewById(R.id.distanceRange);
        txtMin = (TextView) findViewById(R.id.txtMin);
        txtMax = (TextView) findViewById(R.id.txtMax);
        txtDistance = (TextView) findViewById(R.id.txtDistance);

        distanceRange.setOnSeekbarChangeListener(new OnSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue) {
                txtDistance.setText(String.valueOf(minValue));
            }
        });


        rangeSeekBar.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {
                txtMin.setText(String.valueOf(minValue));
                txtMax.setText(String.valueOf(maxValue));
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.swicthShowMeonApp:
                Log.i("switch_compat", isChecked + "");
                break;
            case R.id.swictchShareFeed:
                Log.i("switch_compat2", isChecked + "");
                break;
        }

    }
}
