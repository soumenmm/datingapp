package com.dating.app.activity;

import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.dating.app.R;
import com.dating.app.connection.PostRequestToServer;
import com.dating.app.mycallback.AsyncResponse;
import com.dating.app.utils.Constants;
import com.dating.app.utils.ServerJson;
import com.dating.app.utils.Utils;

import org.json.JSONObject;

import static com.dating.app.utils.SharedKeys.email;

public class ForgetPasswordActivity extends AppCompatActivity {

    private TextInputEditText edit_UserName;
    private String strUserName;
    private Button btnReset;
    private Toolbar toolbar;
    private TextInputLayout editTextLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        edit_UserName = (TextInputEditText) findViewById(R.id.edit_UserName);
        editTextLayout = (TextInputLayout) findViewById(R.id.editTextLayout);


        btnReset = (Button) findViewById(R.id.btnReset);

        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strUserName = edit_UserName.getText().toString();

                if(!Utils.isValidUserName(strUserName))
                {
                    editTextLayout.setError("Please enter valid details");
                }
                if (!strUserName.equalsIgnoreCase("")) {
                    apiCall();
                }

            }
        });

    }

    void apiCall() {
        String json = "";
        if (Utils.isValidMail(strUserName))
            json = ServerJson.getInstance().jsonOTPEmail(strUserName);
        else
            json = ServerJson.getInstance().jsonOTPPhone(strUserName);

        PostRequestToServer postRequestToServer = new PostRequestToServer(json);
        postRequestToServer.delegate = asyncResponse;
        if (Utils.isValidMail(strUserName))
            postRequestToServer.execute(Constants.otpForEmail);
        else
            postRequestToServer.execute(Constants.otpForPhone);

    }

    AsyncResponse asyncResponse = new AsyncResponse() {
        @Override
        public void processFinished(String data) {
            if (!data.equalsIgnoreCase("")) {
                try {
                    JSONObject jsonObject = new JSONObject(data);
                    if (jsonObject.getString("status").equalsIgnoreCase("true")) {
                        Intent intent = new Intent(ForgetPasswordActivity.this, ForgetOTPActivity.class);
                        intent.putExtra("OTP", jsonObject.getString("otp"));
                        intent.putExtra("userName", strUserName);
                        startActivity(intent);
                    }

                } catch (Exception e) {
                }
            }

        }
    };


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
