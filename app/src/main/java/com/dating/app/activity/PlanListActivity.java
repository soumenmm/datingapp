package com.dating.app.activity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.dating.app.R;
import com.dating.app.adapter.PlanListAdapter;
import com.dating.app.connection.RequestToServer;
import com.dating.app.model.plan.PlanResponse;
import com.dating.app.mycallback.AsyncResponse;
import com.dating.app.mycallback.PaytmCustomResponse;
import com.dating.app.utils.Constants;
import com.dating.app.utils.SharedKeys;
import com.dating.app.utils.SharedPreferenceHelper;
import com.google.gson.Gson;

import org.json.JSONObject;

public class PlanListActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private RecyclerView recList;
    private PlanListAdapter adapter;
    private Context context;
    private TextView txtAmount;
    private String walletAmount ="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plan_list);

        initUI();
        getBalance();
        apiCall();
    }

    void initUI(){
        context = this;
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        recList = (RecyclerView) findViewById(R.id.recList);
        txtAmount = (TextView) findViewById(R.id.txtAmount);


       // adapter = new PlanListAdapter(this);
       // recList.setAdapter(adapter);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    void apiCall() {
        RequestToServer requestToServer = new RequestToServer();
        requestToServer.delegate = asyncResponse;
        requestToServer.execute(Constants.planList);

    }


    AsyncResponse asyncResponse = new AsyncResponse() {
        @Override
        public void processFinished(String data) {
            if (data.length()>20){
                try {
                    JSONObject jsonObject = new JSONObject(data);
                    if(jsonObject.getString("status").equalsIgnoreCase("true")) {
                        Gson gson = new Gson();
                        PlanResponse[] planResponse = gson.fromJson(jsonObject.getJSONArray("data").toString(), PlanResponse[].class);
                        adapter = new PlanListAdapter(context,walletAmount,planResponse,paytmCustomResponse);
                        recList.setAdapter(adapter);
                    }

                }catch (Exception e){}
            }

        }
    };


    void getBalance(){
        RequestToServer requestToServer = new RequestToServer();
        requestToServer.delegate = balanceAsyncResponse;
        requestToServer.execute(Constants.getWalletbalance+"/"+SharedPreferenceHelper.get(SharedKeys.userId));
    }


    AsyncResponse balanceAsyncResponse = new AsyncResponse() {
        @Override
        public void processFinished(String data) {
            try {

                JSONObject jsonObject = new JSONObject(data);
                if(jsonObject.getString("status").equalsIgnoreCase("true")){
                    JSONObject jsonObjectData = jsonObject.getJSONObject("data");
                    //walletAmount ="2000" ;jsonObjectData.getString("current_balance");
                    walletAmount =jsonObjectData.getString("current_balance");
                    txtAmount.setText("₹ "+walletAmount);

                }
            }catch (Exception e){}
        }
    };


    PaytmCustomResponse paytmCustomResponse = new PaytmCustomResponse() {
        @Override
        public void payResponse(String response, boolean isSucess) {
            Toast.makeText(PlanListActivity.this,response,Toast.LENGTH_LONG).show();
        }
    };
}
