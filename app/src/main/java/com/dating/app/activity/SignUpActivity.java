package com.dating.app.activity;

import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.dating.app.R;
import com.dating.app.activity.drawer.NavigationActivity;
import com.dating.app.connection.PostRequestToServer;
import com.dating.app.connection.PutRequestToServer;
import com.dating.app.model.signup.SignUpModel;
import com.dating.app.mycallback.AsyncResponse;
import com.dating.app.utils.Constants;
import com.dating.app.utils.ServerJson;
import com.dating.app.utils.SharedKeys;
import com.dating.app.utils.SharedPreferenceHelper;
import com.dating.app.utils.Utils;
import org.json.JSONObject;
import okhttp3.internal.Util;

import static com.dating.app.utils.SharedKeys.email;
import static com.dating.app.utils.SharedKeys.mobileNo;
import static com.dating.app.utils.SharedKeys.name;
import static com.dating.app.utils.SharedKeys.password;

public class SignUpActivity extends AppCompatActivity {

    private TextInputEditText edit_Name;
    private TextInputEditText edit_Email;
    private TextInputEditText edit_Phone;
    private TextInputEditText edit_Password;
    private Button btnContinue;
    private String strEmail, strName, strPhone, strPassword, strID = "";
    private SignUpModel signUpModel;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        initUI();
        onClick();
    }

    void initUI() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        edit_Name = (TextInputEditText) findViewById(R.id.edit_Name);
        edit_Email = (TextInputEditText) findViewById(R.id.edit_Email);
        edit_Phone = (TextInputEditText) findViewById(R.id.edit_Phone);
        edit_Password = (TextInputEditText) findViewById(R.id.edit_Password);
        btnContinue = (Button) findViewById(R.id.btnContinue);
    }

    void onClick() {

        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                strName = edit_Name.getText().toString().trim();
                strEmail = edit_Email.getText().toString().trim();
                strPhone = edit_Phone.getText().toString().trim();
                strPassword = edit_Password.getText().toString().trim();
                signUpModel = new SignUpModel();
                signUpModel.setFullname(strName);
                signUpModel.setEmail(strEmail);
                signUpModel.setPhoneNo(strPhone);
                signUpModel.setPassword(strPassword);

                apiCall();

               /* if (!Utils.validateName(name)) {
                    edit_Name.setError("Enter Valid Name");
                }

                if (!Utils.isValidMail(email)) {
                    edit_Email.setError("Enter valid Email");
                }

                if (!Utils.isValidMobile(mobileNo)) {
                    edit_Phone.setError("Enter valid PhoneNo");
                }

                if (!Utils.isValidPassword(password)) {
                    edit_Password.setError("Enter valid Password");
                }
*/


            }
        });
    }

    void apiCall() {
        String json = ServerJson.getInstance().jsonSignUP(strID, strName, strEmail, strPhone,strPassword);
        if(strID.equalsIgnoreCase("")){
            PostRequestToServer postRequestToServer = new PostRequestToServer(json);
            postRequestToServer.delegate = asyncResponse;
            postRequestToServer.execute(Constants.signUP);
        }else {
            PutRequestToServer putRequestToServer = new PutRequestToServer(json);
            putRequestToServer.delegate = asyncResponse;
            putRequestToServer.execute(Constants.signUP+"/"+strID);
        }

    }

    AsyncResponse asyncResponse = new AsyncResponse() {
        @Override
        public void processFinished(String data) {
            try {
                JSONObject jsonObject = new JSONObject(data);
                if (jsonObject.getString("status").equalsIgnoreCase("true")) {
                    JSONObject myData = jsonObject.getJSONObject("data");
                    strID = myData.getString("id");
                    signUpModel.setId(strID);
                    signUpModel.setOTP(""+myData.getInt("otp"));
                    SharedPreferenceHelper.save(SharedKeys.name, strName);
                    SharedPreferenceHelper.save(SharedKeys.mobileNo, strPhone);
                    SharedPreferenceHelper.save(email, strEmail);
                    Intent intent = new Intent(SignUpActivity.this,VerifyLSinupActivity.class);
                    intent.putExtra("User",signUpModel);
                    startActivity(intent);
                }
            } catch (Exception e) {
                Toast.makeText(SignUpActivity.this, "Technical error", Toast.LENGTH_LONG).show();
            }


        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
