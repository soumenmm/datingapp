package com.dating.app.activity.plans;

import android.content.Context;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dating.app.R;
import com.dating.app.activity.LoginActivity;
import com.dating.app.adapter.PlanAdapter;
import com.dating.app.connection.PostRequestToServer;
import com.dating.app.connection.RequestToServer;
import com.dating.app.model.plan.PlanResponse;
import com.dating.app.model.plan.Plandetail;
import com.dating.app.mycallback.AsyncResponse;
import com.dating.app.mycallback.PaytmCustomResponse;
import com.dating.app.paytm.PaytmImpl;
import com.dating.app.utils.Constants;
import com.dating.app.utils.ServerJson;
import com.dating.app.utils.SharedKeys;
import com.dating.app.utils.SharedPreferenceHelper;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PlanDialog {

    private final ImageView imgClose;
    private final TextView txtRs1;
    private final TextView txtRs2;
    private final TextView txtRs3;
    private final TextView txtDesc1;
    private final TextView txtDesc2;
    private final TextView txtDesc3;
    private final TextView txtPlanName;
    private final TextView txtPlanDesc;
    private final TextView txtValidity1;
    private final TextView txtValidity2;
    private final TextView txtValidity3;
    private final Button btnBuy1;
    private final Button btnBuy2;
    private final Button btnBuy3;
    private Context context;
    private BottomSheetDialog dialog;
    private PlanResponse planResponse;
    private List<Plandetail> planDetails;
    private PaytmCustomResponse paytmCustomResponse;
    private String walletAmount;
    private float intWallet = 0;
    private String planId="";
    private String strPrice="";

    public PlanDialog(Context context, String walletAmount, final PlanResponse planResponse, PaytmCustomResponse paytmCustomResponse) {
        this.context = context;
        this.paytmCustomResponse = paytmCustomResponse;
        this.walletAmount = walletAmount;
        if (!walletAmount.equalsIgnoreCase("") && walletAmount.length() > 0)
            intWallet = Float.valueOf(walletAmount);
        this.planResponse = planResponse;
        dialog = new BottomSheetDialog(context);
        dialog.setContentView(R.layout.plandetails_dialog);
        planDetails = planResponse.getPlandetail();
        imgClose = (ImageView) dialog.findViewById(R.id.imgClose);
        txtPlanName = (TextView) dialog.findViewById(R.id.txtPlanName);
        txtPlanDesc = (TextView) dialog.findViewById(R.id.txtPlanDesc);

        txtValidity1 = (TextView) dialog.findViewById(R.id.txtValidity1);
        txtValidity2 = (TextView) dialog.findViewById(R.id.txtValidity2);
        txtValidity3 = (TextView) dialog.findViewById(R.id.txtValidity3);

        btnBuy1 = (Button) dialog.findViewById(R.id.btnBuy1);
        btnBuy2 = (Button) dialog.findViewById(R.id.btnBuy2);
        btnBuy3 = (Button) dialog.findViewById(R.id.btnBuy3);


        txtRs1 = (TextView) dialog.findViewById(R.id.txtRs1);
        txtRs2 = (TextView) dialog.findViewById(R.id.txtRs2);
        txtRs3 = (TextView) dialog.findViewById(R.id.txtRs3);
        txtDesc1 = (TextView) dialog.findViewById(R.id.txtDesc1);
        txtDesc2 = (TextView) dialog.findViewById(R.id.txtDesc2);
        txtDesc3 = (TextView) dialog.findViewById(R.id.txtDesc3);

        txtPlanName.setText(planResponse.getName());
        txtPlanDesc.setText(planResponse.getAboutplan());
        txtRs1.setText(" ₹ " + planDetails.get(0).getPrice());
        txtRs2.setText(" ₹ " + planDetails.get(1).getPrice());
        txtRs3.setText(" ₹ " + planDetails.get(2).getPrice());

        txtDesc1.setText(planDetails.get(0).getDescription());
        txtDesc2.setText(planDetails.get(1).getDescription());
        txtDesc3.setText(planDetails.get(2).getDescription());

        txtValidity1.setText(planDetails.get(0).getValidity() + " Month");
        txtValidity2.setText(planDetails.get(1).getValidity() + " Month");
        txtValidity3.setText(planDetails.get(2).getValidity() + " Month");

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btnBuy1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float payAmount = Float.valueOf(planDetails.get(0).getPrice());
                planId =""+ planResponse.getPlandetail().get(0).getId();
                float payAable = payAmount - intWallet;
                strPrice = ""+payAmount;
                if (payAable > 0)
                    paytmCall(payAable);
                else
                    planPurchase();
            }
        });

        btnBuy2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float payAmount = Float.valueOf(planDetails.get(1).getPrice());
                float payAable = payAmount - intWallet;
                planId =""+ planResponse.getPlandetail().get(1).getId();
                strPrice = ""+payAmount;
                if (payAable > 0)
                    paytmCall(payAable);
                else
                    planPurchase();
            }
        });


        btnBuy3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float payAmount = Float.valueOf(planDetails.get(2).getPrice());
                planId =""+ planResponse.getPlandetail().get(2).getId();
                strPrice = ""+payAmount;

                float payAable = payAmount - intWallet;
                if (payAable > 0)
                    paytmCall(payAable);
                else
                    planPurchase();
            }
        });

        // apiCall();

    }

    public void show() {
        dialog.show();
    }

    public void hide() {
        dialog.hide();
    }

    void paytmCall(float payAable) {
        PaytmImpl paytmImpl = new PaytmImpl(context, payAable);
        paytmImpl.delegate = paytmCustomResponse;
        //paytmImpl.executePaytm();

    }


    void planPurchase() {
        String json = ServerJson.getInstance().planPurchase("",planId,"","","1");
        PostRequestToServer postRequestToServer = new PostRequestToServer(json);
        postRequestToServer.delegate = asyncResponse;
        postRequestToServer.execute(Constants.planPurchase+SharedPreferenceHelper.get(SharedKeys.userId));
    }

    AsyncResponse asyncResponse  = new AsyncResponse() {
        @Override
        public void processFinished(String data) {
            Toast.makeText(context,data,Toast.LENGTH_LONG).show();
        }
    };


}
