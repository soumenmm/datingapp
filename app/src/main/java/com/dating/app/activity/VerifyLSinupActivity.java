package com.dating.app.activity;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.DragEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.widgets.BubbleThumbRangeSeekbar;
import com.crystal.crystalrangeseekbar.widgets.BubbleThumbSeekbar;
import com.dating.app.R;
import com.dating.app.activity.drawer.NavigationActivity;
import com.dating.app.connection.SignUpToServer;
import com.dating.app.model.login.LoginResponse;
import com.dating.app.model.signup.SignUpModel;
import com.dating.app.mycallback.AsyncResponse;
import com.dating.app.utils.Constants;
import com.dating.app.utils.SharedKeys;
import com.dating.app.utils.SharedPreferenceHelper;
import com.google.gson.Gson;
import com.mukesh.OtpView;
import com.nguyenhoanglam.imagepicker.model.Config;
import com.nguyenhoanglam.imagepicker.model.Image;
import com.nguyenhoanglam.imagepicker.ui.imagepicker.ImagePicker;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import id.zelory.compressor.Compressor;
import info.hoang8f.android.segmented.SegmentedGroup;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class VerifyLSinupActivity extends AppCompatActivity {

    private TextInputEditText editAge;
    private File compressedImage=null;
    private Context context;
    private ImageView imgProfile;
    private ImageView imgAdd;
    private ArrayList<Image> images = new ArrayList<>();
    private SignUpModel signUpModel;
    private OtpView otpView;
    private Button btnContinue;
    private String strOTP,strAge,strGender="";
    private RadioButton radioMale;
    private RadioButton radioFemale;
    private RadioButton radioOther;
    private SegmentedGroup segmentGender;
    private Image image=null;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_lsinup);
        context = this;
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            signUpModel = (SignUpModel) getIntent().getSerializableExtra("User");
        }
        initUI();
    }

    void initUI(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        editAge = (TextInputEditText) findViewById(R.id.editAge);
        imgProfile = (ImageView) findViewById(R.id.imgProfile);
        imgAdd = (ImageView) findViewById(R.id.imgAdd);
        otpView = (OtpView) findViewById(R.id.otpView);
        btnContinue = (Button) findViewById(R.id.btnContinue);
        segmentGender = (SegmentedGroup) findViewById(R.id.segmentGender);
        radioMale = (RadioButton) findViewById(R.id.radioMale);
        radioFemale = (RadioButton) findViewById(R.id.radioFemale);
        radioOther = (RadioButton) findViewById(R.id.radioOther);

        segmentGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb=(RadioButton)findViewById(checkedId);
                strGender = rb.getText().toString();
            }
        });

        imgAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadingImage();
            }
        });

        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strOTP = otpView.getText().toString();
                strAge = editAge.getText().toString();
                if(!strOTP.equalsIgnoreCase(signUpModel.getOTP())){
                    Toast.makeText(context,"Please enter valid OTP",Toast.LENGTH_LONG).show();
                   // return;
                }
                if (strAge.equalsIgnoreCase("")){
                    Toast.makeText(context,"Please enter Age",Toast.LENGTH_LONG).show();
                    return;
                }
                if (strGender.equalsIgnoreCase("")){
                    Toast.makeText(context,"Please select Gender",Toast.LENGTH_LONG).show();
                    return;
                }
                if (image==null){
                    Toast.makeText(context,"Please upload Image file",Toast.LENGTH_LONG).show();
                    return;
                }

                signUpModel.setAge(strAge);
                signUpModel.setGender(strGender);
                signUpModel.setLati("1111");
                signUpModel.setLongi("1111");
                signUpModel.setDeviceId(SharedPreferenceHelper.get(SharedKeys.deviceToken));
                apiCall();
            }
        });
    }

    void uploadingImage() {
        ImagePicker.with(this)
                .setFolderMode(true)
                .setCameraOnly(false)
                .setFolderTitle("Album")
                .setMultipleMode(false)
                .setSelectedImages(images)
                .setMaxSize(1)
                .setBackgroundColor("#212121")
                .setAlwaysShowDoneButton(true)
                .setKeepScreenOn(true)
                .start();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Config.RC_PICK_IMAGES && resultCode == RESULT_OK && data != null) {
            images = data.getParcelableArrayListExtra(Config.EXTRA_IMAGES);
                Glide.with(context)
                        .load(images.get(0).getPath())
                        .apply(new RequestOptions().placeholder(R.drawable.image_placeholder).error(R.drawable.image_placeholder))
                        .into(imgProfile);
                image = images.get(0);
            compressFile(images.get(0));
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    void compressFile(Image image){
        new Compressor(context)
                .compressToFileAsFlowable(new File(image.getPath()))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new io.reactivex.functions.Consumer<File>() {
                    @Override
                    public void accept(File file) {
                        compressedImage = file;
                    }
                }, new io.reactivex.functions.Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) {
                        throwable.printStackTrace();
                        Toast.makeText(context,throwable.getMessage(),Toast.LENGTH_LONG).show();
                    }
                });
    }

    void apiCall(){
        SignUpToServer signUpToServer = new SignUpToServer(image.getName(),new File(image.getPath()),signUpModel);
        signUpToServer.delegate = asyncResponse;
        signUpToServer.execute(Constants.signUpVerify+"/"+signUpModel.getId());
    }

    AsyncResponse asyncResponse = new AsyncResponse() {
        @Override
        public void processFinished(String data) {
            if (data.length() > 20) {
                try {
                    JSONObject jsonObject = new JSONObject(data);
                    Gson gson = new Gson();
                    LoginResponse loginResponse = gson.fromJson(jsonObject.getJSONObject("data").toString(), LoginResponse.class);
                    SharedPreferenceHelper.save(SharedKeys.userName, signUpModel.getEmail());
                    SharedPreferenceHelper.save(SharedKeys.password, signUpModel.getPassword());
                    SharedPreferenceHelper.save(SharedKeys.name, loginResponse.getFullname());
                    SharedPreferenceHelper.save(SharedKeys.userId, loginResponse.getId());
                    SharedPreferenceHelper.save(SharedKeys.mobileNo, loginResponse.getPhoneNo());
                    //  DatingApplication.the().getmAgoraAPI().login2(Constants.AppID, ""+loginResponse.getId(), "_no_need_token", 0, "", 5, 1);

//                    if(loginResponse.getUserprofileimages().get(0)!=null)
//                    SharedPreferenceHelper.save(SharedKeys.profileImage, Constants.rootURL+loginResponse.getUserprofileimages().get(0).getUserProfilePics());
//                    else
//                        SharedPreferenceHelper.save(SharedKeys.profileImage, Constants.rootURL);

                    SharedPreferenceHelper.save(SharedKeys.userData,data);
                    startActivity(new Intent(VerifyLSinupActivity.this, NavigationActivity.class));

                } catch (Exception e) {
                    Toast.makeText(VerifyLSinupActivity.this,"Error"+e,Toast.LENGTH_LONG).show();

                }
            }else
                Toast.makeText(VerifyLSinupActivity.this,"Error",Toast.LENGTH_LONG).show();
        }
    };


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
