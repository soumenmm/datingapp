package com.dating.app.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.dating.app.R;
import com.dating.app.adapter.ImagePager;
import com.dating.app.model.userdetails.UserDetailsModel;
import com.dating.app.model.userlist.DataItem;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class UserDetailActivity extends AppCompatActivity {

    private ViewPager photosViewpager;
    private TabLayout tabLayout;
    private ArrayList<String> item;
    private ImagePager imagePager;
    private Context context;
    private DataItem dataItem;
    private TextInputLayout edit_about;
    private TextInputEditText edit_About;
    private TextInputLayout edit_email;
    private TextInputEditText edit_Email;
    private TextInputLayout edit_age;
    private TextInputEditText edit_Age;
    private TextInputLayout edit_location;
    private TextInputEditText edit_Location;
    private TextInputLayout edit_phoneverification;
    private TextInputEditText edit_PhoneVerification;
    private TextInputLayout edit_bodytype;
    private TextInputEditText edit_BodyType;
    private TextInputLayout edit_maritalstatus;
    private TextInputEditText edit_MaritalStatus;
    private TextInputLayout edit_gender;
    private TextInputEditText edit_Gender;
    private TextInputLayout edit_emailverification;
    private TextInputEditText editEmailVerification;
    private TextInputLayout edit_Current;
    private TextInputEditText edit_CurrentAddress;
    private TextInputLayout edit_PAddress;
    private TextInputEditText edit_PermanentAddress;
    private TextInputLayout edit_EDucation;
    private TextInputEditText edit_Education;
    private TextInputLayout edit_PinCode;
    private TextInputEditText edit_Pincode;
    private TextInputLayout edit_weight;
    private TextInputEditText edit_Weight;
    private TextInputLayout edit_height;
    private TextInputEditText edit_Height;
    private TextInputLayout edit_income;
    private TextInputEditText edit_Income;

    private UserDetailsModel user;
    private Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_userdetail);

        user = (UserDetailsModel) getIntent().getExtras().getSerializable("UserModel");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //toolbar.setTitle(dataItem.getFullname());
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        initUI();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                setData();
            }
        });
    }

    void initUI(){
        context =this;

        edit_about = (TextInputLayout) findViewById(R.id.edit_about);
        edit_email = (TextInputLayout) findViewById(R.id.edit_email);
        edit_age = (TextInputLayout) findViewById(R.id.edit_age);
        edit_location = (TextInputLayout) findViewById(R.id.edit_location);
        edit_phoneverification = (TextInputLayout) findViewById(R.id.edit_phoneverification);
        edit_bodytype = (TextInputLayout) findViewById(R.id.edit_bodytype);
        edit_maritalstatus = (TextInputLayout) findViewById(R.id.edit_maritalstatus);
        edit_gender = (TextInputLayout) findViewById(R.id.edit_gender);
        edit_emailverification = (TextInputLayout) findViewById(R.id.edit_emailverification);
        edit_Current = (TextInputLayout) findViewById(R.id.edit_Current);
        edit_PAddress = (TextInputLayout) findViewById(R.id.edit_PAddress);
        edit_EDucation = (TextInputLayout) findViewById(R.id.edit_EDucation);
        edit_PinCode = (TextInputLayout) findViewById(R.id.edit_PinCode);
        edit_weight = (TextInputLayout) findViewById(R.id.edit_weight);
        edit_height = (TextInputLayout) findViewById(R.id.edit_height);
        edit_income = (TextInputLayout) findViewById(R.id.edit_income);

        edit_About = (TextInputEditText) findViewById(R.id.edit_About);
        edit_Email = (TextInputEditText) findViewById(R.id.edit_Email);
        edit_Age = (TextInputEditText) findViewById(R.id.edit_Age);
        edit_Location = (TextInputEditText) findViewById(R.id.edit_Location);
        edit_PhoneVerification = (TextInputEditText) findViewById(R.id.edit_PhoneVerification);
        edit_BodyType = (TextInputEditText) findViewById(R.id.edit_BodyType);
        edit_MaritalStatus = (TextInputEditText) findViewById(R.id.edit_MaritalStatus);
        edit_Gender = (TextInputEditText) findViewById(R.id.edit_Gender);
        //editEmailVerification = (TextInputEditText) findViewById(R.id.edit_emailverification);
        edit_CurrentAddress = (TextInputEditText) findViewById(R.id.edit_CurrentAddress);
        edit_PermanentAddress = (TextInputEditText) findViewById(R.id.edit_PermanentAddress);
        edit_Education = (TextInputEditText) findViewById(R.id.edit_Education);
        edit_Pincode = (TextInputEditText) findViewById(R.id.edit_Pincode);
        edit_Weight = (TextInputEditText) findViewById(R.id.edit_Weight);
        edit_Height = (TextInputEditText) findViewById(R.id.edit_Height);
        edit_Income = (TextInputEditText) findViewById(R.id.edit_Income);

        photosViewpager = (ViewPager) findViewById(R.id.photosViewpager);
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        imagePager = new ImagePager(context,user);
        photosViewpager.setAdapter(imagePager);
        tabLayout.setupWithViewPager(photosViewpager, true);

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    void setData(){
        if(user.getStrAbout()!=null && !user.getStrAbout().equalsIgnoreCase(""))
            edit_About.setText(user.getStrAbout());

        if(user.getStrEmail()!=null && !user.getStrEmail().equalsIgnoreCase(""))
            edit_Email.setText(user.getStrEmail());

        if(user.getStrAge()!=null && !user.getStrAge().equalsIgnoreCase(""))
            edit_Age.setText(user.getStrAge());

        if(user.getStrEducation()!=null && !user.getStrEducation().equalsIgnoreCase(""))
            edit_Education.setText(user.getStrEducation());

        if(user.getStrPhoneVerification()!=null && !user.getStrPhoneVerification().equalsIgnoreCase(""))
            edit_PhoneVerification.setText(user.getStrPhoneVerification());

        if(user.getStrBodyType()!=null && !user.getStrBodyType().equalsIgnoreCase(""))
            edit_BodyType.setText(user.getStrBodyType());

        if(user.getStrMaritalStatus()!=null && !user.getStrMaritalStatus().equalsIgnoreCase(""))
            edit_MaritalStatus.setText(user.getStrMaritalStatus());

        if(user.getStrGender()!=null && !user.getStrGender().equalsIgnoreCase(""))
            edit_Gender.setText(user.getStrGender());

        if(user.getStrEmail()!=null && !user.getStrEmail().equalsIgnoreCase(""))
            edit_Email.setText(user.getStrEmail());

    }

}
