package com.dating.app.activity;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.dating.app.R;
import com.dating.app.activity.drawer.NavigationActivity;
import com.dating.app.connection.ImageToServer;
import com.dating.app.connection.PutRequestToServer;
import com.dating.app.connection.RequestToServer;
import com.dating.app.model.dropdown.SpinValue;
import com.dating.app.model.editprofile.EditProfileModel;
import com.dating.app.model.userprofile.Data;
import com.dating.app.model.userprofile.UserProfileResponse;
import com.dating.app.model.userprofile.UserprofileimagesItem;
import com.dating.app.mycallback.AsyncResponse;
import com.dating.app.utils.Constants;
import com.dating.app.utils.ServerJson;
import com.dating.app.utils.SharedKeys;
import com.dating.app.utils.SharedPreferenceHelper;
import com.dating.app.utils.Utils;
import com.google.gson.Gson;
import com.nguyenhoanglam.imagepicker.model.Config;
import com.nguyenhoanglam.imagepicker.model.Image;
import com.nguyenhoanglam.imagepicker.ui.imagepicker.ImagePicker;
import android.widget.AdapterView.OnItemSelectedListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import id.zelory.compressor.Compressor;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class EditProfileActivity extends AppCompatActivity {

    private AppCompatSpinner spinState, spinCity, spinCast, spinReligion, spinHobby,spinBodyType,spinMaritalStatus,spinGender;
    private Utils utils;
    private Toolbar toolbar;
    private ArrayList<SpinValue> stateData = new ArrayList<>();
    private ArrayList<SpinValue> cityData = new ArrayList<>();
    private ArrayList<SpinValue> castData = new ArrayList<>();
    private ArrayList<SpinValue> relogionData = new ArrayList<>();
    private ArrayList<SpinValue> hobbyData = new ArrayList<>();
    private ArrayList<SpinValue> bodyTypeData = new ArrayList<>();
    private ArrayList<SpinValue> maritalStatusData = new ArrayList<>();
    private ArrayList<SpinValue> spinGenderData = new ArrayList<>();
    private Context context;
    private String strCityId, strStateId, strHobbyId, strCastId, strReligionId;
    private ImageView img1, img2, img3, img4, img5, img6;
    private ImageView imgProfile1, imgProfile2, imgProfile3, imgProfile4, imgProfile5, imgProfile6;
    private Button btnContinue;
    private ArrayList<Image> images = new ArrayList<>();
    private int imageId=0;
    private File compressedImage;
    private String imageId1="",imageId2="",imageId3="",imageId4="",imageId5="",imageId6="";
    private TextInputEditText edit_About,edit_CurrentAddress,edit_PermanentAddress,edit_Pincode,edit_Weight,edit_Height,edit_Income,edit_Education;
    List<String> list;
    private EditProfileModel editProfile ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        initUI();
        getAPICall();
    }

    void initUI() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        context = this;
        utils = new Utils();
        editProfile = new EditProfileModel();

        spinState = (AppCompatSpinner) findViewById(R.id.spinState);
        spinCity = (AppCompatSpinner) findViewById(R.id.spinCity);
        spinCast = (AppCompatSpinner) findViewById(R.id.spinCast);
        spinReligion = (AppCompatSpinner) findViewById(R.id.spinReligion);
        spinHobby = (AppCompatSpinner) findViewById(R.id.spinHobby);
        spinBodyType = (AppCompatSpinner) findViewById(R.id.spinBodyType);
        spinMaritalStatus = (AppCompatSpinner) findViewById(R.id.spinMaritalStatus);
        spinGender = (AppCompatSpinner) findViewById(R.id.spinGender);
        img1 = (ImageView) findViewById(R.id.img1);
        img2 = (ImageView) findViewById(R.id.img2);
        img3 = (ImageView) findViewById(R.id.img3);
        img4 = (ImageView) findViewById(R.id.img4);
        img5 = (ImageView) findViewById(R.id.img5);
        img6 = (ImageView) findViewById(R.id.img6);
        imgProfile1 = (ImageView) findViewById(R.id.imgProfile1);
        imgProfile2 = (ImageView) findViewById(R.id.imgProfile2);
        imgProfile3 = (ImageView) findViewById(R.id.imgProfile3);
        imgProfile4 = (ImageView) findViewById(R.id.imgProfile4);
        imgProfile5 = (ImageView) findViewById(R.id.imgProfile5);
        imgProfile6 = (ImageView) findViewById(R.id.imgProfile6);

        edit_About = (TextInputEditText) findViewById(R.id.edit_About);
        edit_CurrentAddress = (TextInputEditText) findViewById(R.id.edit_CurrentAddress);
        edit_PermanentAddress = (TextInputEditText) findViewById(R.id.edit_PermanentAddress);
        edit_Pincode = (TextInputEditText) findViewById(R.id.edit_Pincode);
        edit_Weight = (TextInputEditText) findViewById(R.id.edit_Weight);
        edit_Height = (TextInputEditText) findViewById(R.id.edit_Height);
        edit_Income = (TextInputEditText) findViewById(R.id.edit_Income);
        edit_Education = (TextInputEditText) findViewById(R.id.edit_Education);


        btnContinue = (Button) findViewById(R.id.btnContinue);

        img1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadingImage(1);
            }
        });

        img2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadingImage(2);
            }
        });

        img3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadingImage(3);
            }
        });

        img4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadingImage(4);
            }
        });

        img5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadingImage(5);
            }
        });

        img6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadingImage(6);
            }
        });


        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //uploadingImage();
                //Go to home page
                //putAPICallProfileEditing();
                editProfile.setAboutYou(edit_About.getText().toString().trim());
                editProfile.setCurrentAddress(edit_CurrentAddress.getText().toString().trim());
                editProfile.setPermanentAddress(edit_PermanentAddress.getText().toString().trim());
                editProfile.setEducation(edit_Education.getText().toString().trim());
                editProfile.setPinCode(edit_Pincode.getText().toString().trim());
                editProfile.setState(strStateId);
                editProfile.setCity(strCityId);
                editProfile.setReligion(strReligionId);
                // Weight
                editProfile.setHeight(edit_Height.getText().toString().trim());
                editProfile.setWeight(edit_Weight.getText().toString().trim());
                editProfile.setIncome(edit_Income.getText().toString().trim());

                Gson gson = new Gson();
                String profile = gson.toJson(editProfile);

            }
        });


        try {
            stateData = utils.getSpinValue(Constants.states);
            spinState.setAdapter(utils.getDropDownAdapter(context, stateData));

            cityData = utils.getCityOrCast(Constants.cities, "");
            spinCity.setAdapter(utils.getDropDownAdapter(context, cityData));

            castData = utils.getCityOrCast(Constants.casts, "");
            spinCast.setAdapter(utils.getDropDownAdapter(context, castData));

            relogionData = utils.getSpinValue(Constants.religion);
            spinReligion.setAdapter(utils.getDropDownAdapter(context, relogionData));

            hobbyData = utils.getSpinValue(Constants.hobby);
            spinHobby.setAdapter(utils.getDropDownAdapter(context, hobbyData));



        } catch (Exception e) {
        }

        list = new ArrayList<String> ();
        list.add("Slim");
        list.add("Faty");
        list.add("Average");
        list.add("Athletic");

        ArrayAdapter<String> adp1 = new ArrayAdapter<String>
                (this, android.R.layout.simple_dropdown_item_1line, list);

        spinBodyType.setAdapter(adp1);


        spinBodyType.setOnItemSelectedListener(new OnItemSelectedListener() {

            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                // TODO Auto-generated method stub

               /* Toast.makeText(getBaseContext(), list.get(arg2),
                        Toast.LENGTH_SHORT).show();*/
            }

            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });

        list = new ArrayList<String> ();
        list.add("Married");
        list.add("Unmarried");
        list.add("Single");
        list.add("Widow");

        ArrayAdapter<String> adp2 = new ArrayAdapter<String>
                (this, android.R.layout.simple_dropdown_item_1line, list);

        spinMaritalStatus.setAdapter(adp2);
        spinMaritalStatus.setOnItemSelectedListener(new OnItemSelectedListener() {

            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                // TODO Auto-generated method stub

              /*  Toast.makeText(getBaseContext(), list.get(arg2),
                        Toast.LENGTH_SHORT).show();*/
            }

            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });

        list = new ArrayList<String> ();
        list.add("Male");
        list.add("Female");
        list.add("Transgender");
        list.add("Others");

        ArrayAdapter<String> adp3 = new ArrayAdapter<String>
                (this, android.R.layout.simple_dropdown_item_1line, list);

        spinGender.setAdapter(adp3);


        spinGender.setOnItemSelectedListener(new OnItemSelectedListener() {

            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                // TODO Auto-generated method stub

             //   Toast.makeText(getBaseContext(), list.get(arg2),Toast.LENGTH_SHORT).show();
            }

            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });

        spinState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                strStateId = stateData.get(position).getId();
                cityData = utils.getCityOrCast(Constants.cities, strStateId);
                spinCity.setAdapter(utils.getDropDownAdapter(context, cityData));


                //Toast.makeText(EditProfileActivity.this, strStateId, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                strStateId = stateData.get(position).getId();
                //Toast.makeText(EditProfileActivity.this, strStateId, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinHobby.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                strStateId = stateData.get(position).getId();
                //Toast.makeText(EditProfileActivity.this, strStateId, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinReligion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                castData = utils.getCityOrCast(Constants.casts, relogionData.get(position).getId());
                spinCast.setAdapter(utils.getDropDownAdapter(context, castData));
               // Toast.makeText(EditProfileActivity.this, strCastId, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // set religion constants

    }

    void getAPICall() {
        RequestToServer requestToServer = new RequestToServer();
        requestToServer.delegate = asyncResponse;
        requestToServer.execute(Constants.userProfileSetting + "/"+SharedPreferenceHelper.get(SharedKeys.userId));
    }

//    void putAPICallProfileEditing(String id) {
//        String json = ServerJson.getInstance().jsonProfileEditing(id);
//        PutRequestToServer requestToServer = new PutRequestToServer(json);
//        requestToServer.delegate = asyncResponse;
//        requestToServer.execute(Constants.userProfileSetting + "/"+SharedPreferenceHelper.get(SharedKeys.userId));
//    }

    AsyncResponse asyncResponse = new AsyncResponse() {
        @Override
        public void processFinished(String data) {
            if (data.length() > 10) {
                Gson gson = new Gson();
                UserProfileResponse userProfileResponse = gson.fromJson(data, UserProfileResponse.class);

                setData(userProfileResponse);
            }

        }
    };

    void setData(UserProfileResponse userProfileResponse) {
        Data data =userProfileResponse.getData();
        edit_About.setText((data.getAboutYou()==null)? "" : ""+data.getAboutYou());
        edit_CurrentAddress.setText((data.getCurrentAddress()==null)? "" : ""+data.getCurrentAddress());
        edit_PermanentAddress.setText((data.getPermanentAddress()==null)? "" : ""+data.getPermanentAddress());
        edit_Pincode.setText((data.getPinCode()==null)? "" : ""+data.getPinCode());
        edit_Weight.setText((data.getWeight()==null)? "" : ""+data.getWeight());
        edit_Height.setText((data.getHeight()==null)? "" : ""+data.getHeight());
        edit_Income.setText((data.getIncome()==null)? "" : ""+data.getIncome());
        //edit_Education.setText((data.get()==null)? "" : ""+data.getGender());
        List<UserprofileimagesItem> imageItem = userProfileResponse.getData().getUserprofileimages();
        try {
            for (int i = 0; i < imageItem.size(); i++) {
                if (i == 0) {
                    imageId1 = ""+imageItem.get(i).getId();
                    Glide.with(context)
                            .load(Constants.rootURL + imageItem.get(i).getUserProfilePics())
                            .apply(new RequestOptions().placeholder(R.drawable.image_placeholder).error(R.drawable.image_placeholder))
                            .into(imgProfile1);
                }
                if (i == 1) {
                    imageId2 = ""+imageItem.get(i).getId();
                    Glide.with(context)
                            .load(Constants.rootURL + imageItem.get(i).getUserProfilePics())
                            .apply(new RequestOptions().placeholder(R.drawable.image_placeholder).error(R.drawable.image_placeholder))
                            .into(imgProfile2);
                }
                if (i == 2) {
                    imageId3 = ""+imageItem.get(i).getId();
                    Glide.with(context)
                            .load(Constants.rootURL + imageItem.get(i).getUserProfilePics())
                            .apply(new RequestOptions().placeholder(R.drawable.image_placeholder).error(R.drawable.image_placeholder))
                            .into(imgProfile3);
                }
                if (i == 3) {
                    imageId4 = ""+imageItem.get(i).getId();
                    Glide.with(context)
                            .load(Constants.rootURL + imageItem.get(i).getUserProfilePics())
                            .apply(new RequestOptions().placeholder(R.drawable.image_placeholder).error(R.drawable.image_placeholder))
                            .into(imgProfile4);
                }
                if (i == 4) {
                    imageId5 = ""+imageItem.get(i).getId();
                    Glide.with(context)
                            .load(Constants.rootURL + imageItem.get(i).getUserProfilePics())
                            .apply(new RequestOptions().placeholder(R.drawable.image_placeholder).error(R.drawable.image_placeholder))
                            .into(imgProfile5);
                }
                if (i == 5) {
                    imageId6 = ""+imageItem.get(i).getId();
                    Glide.with(context)
                            .load(Constants.rootURL + imageItem.get(i).getUserProfilePics())
                            .apply(new RequestOptions().placeholder(R.drawable.image_placeholder).error(R.drawable.image_placeholder))
                            .into(imgProfile6);
                }
            }
        } catch (Exception e) {
        }
    }
    void uploadingImage(int id) {
        imageId=id;
        ImagePicker.with(this)
                .setFolderMode(true)
                .setCameraOnly(false)
                .setFolderTitle("Album")
                .setMultipleMode(false)
                .setSelectedImages(images)
                .setMaxSize(1)
                .setBackgroundColor("#212121")
                .setAlwaysShowDoneButton(true)
                .setKeepScreenOn(true)
                .start();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Config.RC_PICK_IMAGES && resultCode == RESULT_OK && data != null) {
            images = data.getParcelableArrayListExtra(Config.EXTRA_IMAGES);
            String imgID="";
            if(imageId==1){
                imgID = imageId1;
                Glide.with(context)
                        .load(images.get(0).getPath())
                        .apply(new RequestOptions().placeholder(R.drawable.image_placeholder).error(R.drawable.image_placeholder))
                        .into(imgProfile1);
            }else if(imageId==2){
                imgID = imageId2;
                Glide.with(context)
                        .load(images.get(0).getPath())
                        .apply(new RequestOptions().placeholder(R.drawable.image_placeholder).error(R.drawable.image_placeholder))
                        .into(imgProfile2);
            }else if(imageId==3){
                imgID = imageId3;
                Glide.with(context)
                        .load(images.get(0).getPath())
                        .apply(new RequestOptions().placeholder(R.drawable.image_placeholder).error(R.drawable.image_placeholder))
                        .into(imgProfile3);
            }else if(imageId==4){
                imgID = imageId4;
                Glide.with(context)
                        .load(images.get(0).getPath())
                        .apply(new RequestOptions().placeholder(R.drawable.image_placeholder).error(R.drawable.image_placeholder))
                        .into(imgProfile4);
            }else if(imageId==5){
                imgID = imageId5;
                Glide.with(context)
                        .load(images.get(0).getPath())
                        .apply(new RequestOptions().placeholder(R.drawable.image_placeholder).error(R.drawable.image_placeholder))
                        .into(imgProfile5);
            }else if(imageId==6){
                imgID = imageId6;
                Glide.with(context)
                        .load(images.get(0).getPath())
                        .apply(new RequestOptions().placeholder(R.drawable.image_placeholder).error(R.drawable.image_placeholder))
                        .into(imgProfile6);
            }
            compressFile(images.get(0),imgID);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    void compressFile(final Image image, final String imgID){
        new Compressor(context)
                .compressToFileAsFlowable(new File(image.getPath()))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new io.reactivex.functions.Consumer<File>() {
                    @Override
                    public void accept(File file) {
                        compressedImage = file;
                        imageSendToserver(image,file,imgID);
                    }
                }, new io.reactivex.functions.Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) {
                        throwable.printStackTrace();
                      //  Toast.makeText(context,throwable.getMessage(),Toast.LENGTH_LONG).show();
                    }
                });
    }

    void imageSendToserver(Image image,File file,String imgID){
        ImageToServer imageToServer = new ImageToServer(image.getName(),file);
        imageToServer.delegate = asyncImageResponse;
        if(imgID.equalsIgnoreCase(""))
        imageToServer.execute(Constants.uploadImages);
        else
            imageToServer.execute(Constants.replaceImage+"/"+imgID);
    }

    AsyncResponse asyncImageResponse = new AsyncResponse() {
        @Override
        public void processFinished(String data) {
            Log.d("Data",data);
        }
    };
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    void updateProfile(String json){
        PutRequestToServer putRequestToServer = new PutRequestToServer(json);
        putRequestToServer.delegate = updateAsyncResponse;
        putRequestToServer.execute(Constants.planList);

    }


    AsyncResponse updateAsyncResponse = new AsyncResponse() {
        @Override
        public void processFinished(String data) {

        }
    };
}
