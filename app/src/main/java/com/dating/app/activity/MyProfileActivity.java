package com.dating.app.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.dating.app.R;
import com.dating.app.activity.customview.CircularImageView;
import com.dating.app.connection.GetRequestToServer;
import com.dating.app.connection.RequestToServer;
import com.dating.app.mycallback.AsyncResponse;
import com.dating.app.utils.Constants;
import com.dating.app.utils.SharedKeys;
import com.dating.app.utils.SharedPreferenceHelper;
import com.squareup.picasso.Picasso;

public class MyProfileActivity extends AppCompatActivity {

    private LinearLayout linearSetting;
    private Toolbar toolbar;
    private LinearLayout linearEdit;
    private CircularImageView imgImage;
    private TextView txtName;
    private TextView txtDesignation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);
        SharedPreferenceHelper.initialize(this);
        initUI();

        linearSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MyProfileActivity.this,SettingActivity.class));
            }
        });

        linearEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MyProfileActivity.this,EditProfileActivity.class));
            }
        });
    }

    void initUI(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        linearSetting = (LinearLayout) findViewById(R.id.linearSetting);
        linearEdit = (LinearLayout) findViewById(R.id.linearEdit);
        imgImage = (CircularImageView) findViewById(R.id.imgImage);
        txtName = (TextView) findViewById(R.id.txtName);
        txtDesignation = (TextView) findViewById(R.id.txtDesignation);

        txtName.setText(SharedPreferenceHelper.get(SharedKeys.name));
        txtDesignation.setText(SharedPreferenceHelper.get(SharedKeys.email));

        //Picasso.get().load(Constants.rootURL + SharedPreferenceHelper.get(SharedKeys.profileImage)).into(imgImage);


        /*Glide.with(this)
                .load(Constants.rootURL + SharedPreferenceHelper.get(SharedKeys.profileImage))
                .apply(new RequestOptions().placeholder(R.drawable.avatar).error(R.drawable.avatar))
                .into(imgImage);*/


        dropDownList();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    void dropDownList(){
        RequestToServer getRequestToServer = new RequestToServer();
        getRequestToServer.delegate = asyncDropDown;
        getRequestToServer.execute(Constants.dropDownList);

    }

    AsyncResponse asyncDropDown = new AsyncResponse() {
        @Override
        public void processFinished(String data) {
            if(!data.equalsIgnoreCase("")){
                SharedPreferenceHelper.save(SharedKeys.dropDownList,data);
            }
        }
    };



}
