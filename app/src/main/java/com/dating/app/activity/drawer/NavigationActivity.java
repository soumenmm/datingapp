package com.dating.app.activity.drawer;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.dating.app.R;
import com.dating.app.activity.MyProfileActivity;
import com.dating.app.activity.PlanListActivity;
import com.dating.app.activity.chat.ChatListActivity;
import com.dating.app.fragments.MyFriendListFragment;
import com.dating.app.fragments.MyRequestFragment;
import com.dating.app.fragments.UserListFragment;
import com.dating.app.fragments.WishListFragment;
import com.dating.app.fragments.WishListGridFragment;
import com.dating.app.utils.Constants;
import com.dating.app.utils.SharedKeys;
import com.dating.app.utils.SharedPreferenceHelper;
import com.infideap.drawerbehavior.AdvanceDrawerLayout;

public class NavigationActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    //private Advance3DDrawerLayout drawer;
    private AdvanceDrawerLayout drawer;
    private ImageView imgProfile;
    private TextView txtName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);;


        drawer = (AdvanceDrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);

        imgProfile = (ImageView) headerView.findViewById(R.id.imgProfile);
        txtName = (TextView) headerView.findViewById(R.id.txtName);

        Glide.with(this)
                .load(Constants.rootURL + SharedPreferenceHelper.get(SharedKeys.profileImage))
                .apply(new RequestOptions().placeholder(R.drawable.avatar).error(R.drawable.avatar))
                .into(imgProfile);

        txtName.setText(SharedPreferenceHelper.get(SharedKeys.name));

        navigationView.setNavigationItemSelectedListener(this);

        navigationView.getMenu().getItem(0).setChecked(true);
        onNavigationItemSelected(navigationView.getMenu().getItem(0));

        drawer.setViewScale(Gravity.START, 0.9f);
        drawer.setRadius(Gravity.START, 35);
        drawer.setViewElevation(Gravity.START, 20);
        //drawer.setViewRotation(Gravity.START, 15);

    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Fragment fragment = null;

        if (id == R.id.nav_profile) {
            startActivity(new Intent(NavigationActivity.this, MyProfileActivity.class));
        }
        if (id == R.id.nav_Chat) {
            startActivity(new Intent(NavigationActivity.this, ChatListActivity.class));
        }
        if (id == R.id.nav_Home) {
            fragment = new UserListFragment();
        }
        if (id == R.id.nav_WishList) {
            fragment = new WishListGridFragment();
        }
        if (id == R.id.nav_MyFriends) {
            fragment = new MyFriendListFragment();
        }
        if (id == R.id.nav_request) {
            fragment = new MyRequestFragment();
        }
        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.layoutContent, fragment).commit();

        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.navigation, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (item.getItemId()) {
            case R.id.action_plan:
                startActivity(new Intent(NavigationActivity.this, PlanListActivity.class));
                break;
            case R.id.action_boost:
                startActivity(new Intent(NavigationActivity.this, PlanListActivity.class));
                break;

            case R.id.action_notification:
                startActivity(new Intent(NavigationActivity.this, PlanListActivity.class));
                break;
            case R.id.action_search:
                startActivity(new Intent(NavigationActivity.this, PlanListActivity.class));
                break;

        }
        return super.onOptionsItemSelected(item);

       /* // Inflate menu resource file.
        getMenuInflater().inflate(R.menu.share_menu, menu);

        // Locate MenuItem with ShareActionProvider
        MenuItem item = menu.findItem(R.id.menu_item_share);

        // Fetch and store ShareActionProvider
        mShareActionProvider = (ShareActionProvider) item.getActionProvider();

        // Return true to display menu
        return true;
    }

    // Call to update the share intent
    private void setShareIntent(Intent shareIntent) {
        if (mShareActionProvider != null) {
            mShareActionProvider.setShareIntent(shareIntent);
        }*/



    }

}
