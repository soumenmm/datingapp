package com.dating.app.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.location.Location;
import android.location.LocationManager;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.dating.app.R;
import com.dating.app.activity.drawer.NavigationActivity;
import com.dating.app.connection.PostRequestToServer;
import com.dating.app.model.login.LoginResponse;
import com.dating.app.mycallback.AsyncResponse;
import com.dating.app.mycallback.PaytmCustomResponse;
import com.dating.app.paytm.PaytmImpl;
import com.dating.app.utils.Constants;
import com.dating.app.utils.ServerJson;
import com.dating.app.utils.SharedKeys;
import com.dating.app.utils.SharedPreferenceHelper;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.vlonjatg.progressactivity.ProgressLinearLayout;

import org.json.JSONObject;

import java.util.List;

public class LoginActivity extends AppCompatActivity {

    private TextView txtSignUP;
    private TextView txtForgetPass;
    private Button btn_Login;
    private TextInputEditText edit_Email;
    private TextInputEditText edit_Password;
    private String strUserName, strPassword, strLat = "", strLong = "", strDeviceID = "";
    private ProgressLinearLayout progressActivity;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        SharedPreferenceHelper.initialize(this);
        initUI();
        FirebaseMessaging.getInstance().setAutoInitEnabled(true);

        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.CAMERA,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                ).withListener(new MultiplePermissionsListener() {
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {/* ... */}

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {/* ... */}
        }).check();

    }

    void initUI() {
        //progressActivity = (ProgressLinearLayout) findViewById(R.id.progressActivity);
        edit_Email = (TextInputEditText) findViewById(R.id.editEmail);
        edit_Password = (TextInputEditText) findViewById(R.id.editPassword);
        btn_Login = (Button) findViewById(R.id.btn_Login);
        txtSignUP = (TextView) findViewById(R.id.txtSignUP);
        txtForgetPass = (TextView) findViewById(R.id.txtForgetPass);
        txtSignUP.setPaintFlags(txtSignUP.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        txtForgetPass.setPaintFlags(txtForgetPass.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        txtForgetPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, ForgetPasswordActivity.class));
            }
        });
        txtSignUP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, SignUpActivity.class));
            }
        });

        btn_Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                if (!Utils.isValidMail(strUserName)) {
//                    edit_Email.setError("Enter valid Email");
//                }

                //PlanDialog planDialog = new PlanDialog(LoginActivity.this);
                //  planDialog.show();
                // startActivity(new Intent(LoginActivity.this,UserDetailActivity.class));
                strUserName = edit_Email.getText().toString().trim();
                strPassword = edit_Password.getText().toString().trim();
                // paytmCall();
               apiCall();
                 //startActivity(new Intent(LoginActivity.this,PlanListActivity.class));

            }

        });
    }

    void apiCall() {

        //progressActivity.showLoading();

        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        try {
            strLat = "" + location.getLongitude();
            strLong = "" + location.getLatitude();
        }
             catch(Exception e)
             {

             }

        String json = ServerJson.getInstance().jsonLogin(strUserName, strPassword, strLat, strLong);
        PostRequestToServer postRequestToServer = new PostRequestToServer(json);
        postRequestToServer.delegate = asyncResponse;
        postRequestToServer.execute(Constants.login);


    }

    AsyncResponse asyncResponse = new AsyncResponse() {
        @Override
        public void processFinished(String data) {
           // progressActivity.showContent();

            if (data.length() > 20) {
                try {
                    JSONObject jsonObject = new JSONObject(data);
                    if(jsonObject.getString("status").equalsIgnoreCase("true")) {
                        Gson gson = new Gson();
                        LoginResponse loginResponse = gson.fromJson(jsonObject.getJSONObject("data").toString(), LoginResponse.class);
                        SharedPreferenceHelper.save(SharedKeys.userName, strUserName);
                        SharedPreferenceHelper.save(SharedKeys.password, strPassword);
                        SharedPreferenceHelper.save(SharedKeys.name, loginResponse.getFullname());
                        SharedPreferenceHelper.save(SharedKeys.userId, loginResponse.getId());
                        SharedPreferenceHelper.save(SharedKeys.mobileNo, loginResponse.getPhoneNo());
                        //  DatingApplication.the().getmAgoraAPI().login2(Constants.AppID, ""+loginResponse.getId(), "_no_need_token", 0, "", 5, 1);

//                    if(loginResponse.getUserprofileimages().get(0)!=null)
//                    SharedPreferenceHelper.save(SharedKeys.profileImage, Constants.rootURL+loginResponse.getUserprofileimages().get(0).getUserProfilePics());
//                    else
//                        SharedPreferenceHelper.save(SharedKeys.profileImage, Constants.rootURL);

                        SharedPreferenceHelper.save(SharedKeys.userData, data);
                        startActivity(new Intent(LoginActivity.this, NavigationActivity.class));
                    }else {
                        Toast.makeText(LoginActivity.this,jsonObject.getString("message"),Toast.LENGTH_LONG).show();
                    }

                } catch (Exception e) {
                    Toast.makeText(LoginActivity.this,"Error"+e,Toast.LENGTH_LONG).show();

                }
            }else
                Toast.makeText(LoginActivity.this,"Error",Toast.LENGTH_LONG).show();

        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        edit_Email.setText(SharedPreferenceHelper.get(SharedKeys.userName));
        edit_Password.setText(SharedPreferenceHelper.get(SharedKeys.password));
    }


   /* void paytmCall(){
        PaytmImpl paytmImpl = new PaytmImpl(LoginActivity.this);
        paytmImpl.delegate = paytmCustomResponse;
       // paytmImpl.executePaytm();

    }*/

    PaytmCustomResponse paytmCustomResponse = new PaytmCustomResponse() {
        @Override
        public void payResponse(String response, boolean isSucess) {
            Toast.makeText(LoginActivity.this,response,Toast.LENGTH_LONG).show();
        }
    };
}
