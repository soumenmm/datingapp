package com.dating.app.activity;

import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.support.annotation.FloatRange;
import android.support.design.widget.BottomSheetDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dating.app.R;
import com.dating.app.utils.SharedPreferenceHelper;
import com.shashank.sony.fancywalkthroughlib.FancyWalkthroughActivity;
import com.shashank.sony.fancywalkthroughlib.FancyWalkthroughCard;
import java.util.ArrayList;
import java.util.List;


public class  MainIntroActivity  extends FancyWalkthroughActivity {

    private TextView txtLogin;
    private Button btnSignUp;
    private ImageView imgClose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferenceHelper.initialize(this);
        FancyWalkthroughCard fancywalkthroughCard1 = new FancyWalkthroughCard("Find your Partner", "Find the best partner in your neighborhood.",R.drawable.aa);
        FancyWalkthroughCard fancywalkthroughCard2 = new FancyWalkthroughCard("Pick the best", "Find the best partner in your neighborhood.",R.drawable.ax);
        FancyWalkthroughCard fancywalkthroughCard3 = new FancyWalkthroughCard("Choose your Partner", "Find the best partner in your neighborhood.",R.drawable.ab);
        FancyWalkthroughCard fancywalkthroughCard4 = new FancyWalkthroughCard("Find your Partner", "Find the best partner in your neighborhood.",R.drawable.an);

        fancywalkthroughCard1.setBackgroundColor(R.color.white);
        fancywalkthroughCard1.setIconLayoutParams(700,700,0,0,0,0);
        fancywalkthroughCard2.setBackgroundColor(R.color.white);
        fancywalkthroughCard2.setIconLayoutParams(700,700,0,0,0,0);
        fancywalkthroughCard3.setBackgroundColor(R.color.white);
        fancywalkthroughCard3.setIconLayoutParams(700,700,0,0,0,0);
        fancywalkthroughCard4.setBackgroundColor(R.color.white);
        fancywalkthroughCard4.setIconLayoutParams(700,700,0,0,0,0);
        List<FancyWalkthroughCard> pages = new ArrayList<>();

        pages.add(fancywalkthroughCard1);
        pages.add(fancywalkthroughCard2);
        pages.add(fancywalkthroughCard3);
        pages.add(fancywalkthroughCard4);

        for (FancyWalkthroughCard page : pages) {
            page.setTitleColor(R.color.black);
            page.setDescriptionColor(R.color.black);
        }
        setFinishButtonTitle("Get Started");
        showNavigationControls(true);
        //setColorBackground(R.color.colorPrimary);
        setImageBackground(R.drawable.gradient_bg);
        setInactiveIndicatorColor(R.color.grey_600);
        setActiveIndicatorColor(R.color.colorPrimaryDark);
        setOnboardPages(pages);

    }

    @Override
    public void onFinishButtonPressed() {
       // Toast.makeText(this, "Finish Pressed", Toast.LENGTH_SHORT).show();
        bottomSheet();

    }

    void bottomSheet(){
        final BottomSheetDialog dialog = new BottomSheetDialog(MainIntroActivity.this);
        dialog.setContentView(R.layout.login_popup);
        txtLogin = (TextView) dialog.findViewById(R.id.txtLogin);
        btnSignUp = (Button) dialog.findViewById(R.id.btnSignUp);
        imgClose = (ImageView) dialog.findViewById(R.id.imgClose);

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        txtLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainIntroActivity.this,LoginActivity.class));

            }
        });

        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainIntroActivity.this,SignUpActivity.class));
            }
        });
        dialog.show();
    }
}
