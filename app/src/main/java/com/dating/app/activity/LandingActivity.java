package com.dating.app.activity;
import android.content.Intent;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dating.app.R;
import com.dating.app.adapter.SliderAdapter;
import com.dating.app.utils.SharedPreferenceHelper;

public class LandingActivity extends AppCompatActivity {

   // private Button btnGoogle;
  //  private Button btnFacebook;
    private Button btnPhone;
    private Button btnEmail;

    private ViewPager mSlideViewPager;
    private LinearLayout mDotsLayout;
    private TextView[] mDots;
    private SliderAdapter sliderAdapter;
    private int mCurrentPage;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing);
        SharedPreferenceHelper.initialize(this);
        initUI();
        onClick();
    }


    void initUI() {
     //   btnGoogle = (Button) findViewById(R.id.btnGoogle);
      //  btnFacebook = (Button) findViewById(R.id.btnFacebook);
        btnPhone = (Button) findViewById(R.id.btnPhone);
        btnEmail = (Button) findViewById(R.id.btnEmail);

        mSlideViewPager = findViewById(R.id.slide_viewpager);
        mDotsLayout = findViewById(R.id.dots_layout);

        sliderAdapter = new SliderAdapter(this);

        mSlideViewPager.setAdapter(sliderAdapter);
        addDotsIndicator(0);
        mSlideViewPager.addOnPageChangeListener(viewListener);

    }

    void onClick() {
        // Name change
        btnPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheet();
               // startActivity(new Intent(LandingActivity.this, LoginOTPActivity.class));
            }
        });

        btnEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LandingActivity.this, LoginActivity.class));
            }
        });
    }


    public void addDotsIndicator(int position) {

        mDots = new TextView[5];
        mDotsLayout.removeAllViews(); //without this multiple number of dots will be created

        for (int i = 0; i < mDots.length; i++) {
            mDots[i] = new TextView(this);
            mDots[i].setText(Html.fromHtml("&#8226;")); //code for the dot icon like thing
            mDots[i].setTextSize(35);
            //  mDots[i].setText("555555");
            mDots[i].setTextColor(getResources().getColor(R.color.red));

            mDotsLayout.addView(mDots[i]);
        }

        if (mDots.length > 0) {
            mDots[position].setTextColor(getResources().getColor(R.color.white)); //setting currently selected dot to white
        }
    }

    ViewPager.OnPageChangeListener viewListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {

            addDotsIndicator(position);

            mCurrentPage = position;


        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    void bottomSheet(){
        BottomSheetDialog dialog = new BottomSheetDialog(LandingActivity.this);
        dialog.setContentView(R.layout.login_popup);
        dialog.show();
    }
}
