package com.dating.app.activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.dating.app.R;
import com.dating.app.connection.PostRequestToServer;
import com.dating.app.mycallback.AsyncResponse;
import com.dating.app.utils.Constants;
import com.dating.app.utils.ServerJson;
import com.dating.app.utils.Utils;

import org.json.JSONObject;

import static com.dating.app.utils.SharedKeys.email;
import static com.dating.app.utils.SharedKeys.password;

public class ForgetOTPActivity extends AppCompatActivity {

    private String strUserName, strPassword;
    private String OTP;
    private TextView txtResend;
    private TextView btnReset;
    private TextInputEditText edit_Otp,edit_Password;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgetotp);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        txtResend = (TextView) findViewById(R.id.txtResend);
        btnReset = (TextView) findViewById(R.id.btnReset);
        edit_Password = (TextInputEditText) findViewById(R.id.edit_Password);
        edit_Otp = (TextInputEditText) findViewById(R.id.edit_Otp);
         strUserName = getIntent().getExtras().getString("userName");
       OTP = getIntent().getExtras().getString("OTP");

        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strPassword = edit_Password.getText().toString();
                OTP = edit_Otp.getText().toString();
                if (!strPassword.equalsIgnoreCase("")) {
                    apiCall();
                }

                if (!OTP.equalsIgnoreCase("")) {
                    apiCall();
                }

                if (!Utils.isValidPassword(password)) {
                    edit_Password.setError("Enter valid Password");
                }

            }
        });

        txtResend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                apiResendCall();
            }
        });
    }

    void apiCall() {
        String json = ServerJson.getInstance().jsonForgetPassword(strUserName, strPassword, OTP);
        PostRequestToServer postRequestToServer = new PostRequestToServer(json);
        postRequestToServer.delegate = asyncResponse;
        postRequestToServer.execute(Constants.login);
        startActivity(new Intent(ForgetOTPActivity.this, LoginActivity.class));

    }

    void apiResendCall() {
        String json = "";
        if (Utils.isValidMail(strUserName))
            json = ServerJson.getInstance().jsonOTPEmail(strUserName);
        else
            json = ServerJson.getInstance().jsonOTPPhone(strUserName);
        PostRequestToServer postRequestToServer = new PostRequestToServer(json);
        postRequestToServer.delegate = asyncResendResponse;
        if (Utils.isValidMail(strUserName))
            postRequestToServer.execute(Constants.otpForEmail);
        else
            postRequestToServer.execute(Constants.otpForPhone);

    }

    AsyncResponse asyncResponse = new AsyncResponse() {
        @Override
        public void processFinished(String data) {
            if (!data.equalsIgnoreCase("")) {
                try {
                    JSONObject jsonObject = new JSONObject(data);
                    Toast.makeText(ForgetOTPActivity.this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();

                } catch (Exception e) {
                }
            }

        }
    };


    AsyncResponse asyncResendResponse = new AsyncResponse() {
        @Override
        public void processFinished(String data) {
            if (!data.equalsIgnoreCase("")) {
                try {
                    JSONObject jsonObject = new JSONObject(data);
                    if (jsonObject.getString("status").equalsIgnoreCase("true")) {
                        OTP = jsonObject.getString("otp");
                    }

                } catch (Exception e) {
                }
            }
        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
