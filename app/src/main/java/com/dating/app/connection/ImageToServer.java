package com.dating.app.connection;

import android.os.AsyncTask;

import com.dating.app.mycallback.AsyncResponse;
import com.dating.app.utils.SharedKeys;
import com.dating.app.utils.SharedPreferenceHelper;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Bishnu on 30-07-2018.
 */
public class ImageToServer extends AsyncTask<String,String,String> {

    public AsyncResponse delegate=null;
    private String userId="";
    private String fileName="";
    private File file;

    public ImageToServer(String fileName,File file) {
        this.file=file;
        this.fileName=fileName;
        this.userId=SharedPreferenceHelper.get(SharedKeys.mobileNo);
    }

    @Override
    protected String doInBackground(String... urls) {
        String return_response = "";
        try {
            for (String url : urls) {
                final MediaType MEDIA_TYPE_PNG = MediaType.parse("image/png");

                RequestBody req = new MultipartBody.Builder()
                        .setType(MultipartBody.FORM)
                        .addFormDataPart("phone_no", userId)
                        .addFormDataPart("files",fileName, RequestBody.create(MEDIA_TYPE_PNG,file))
                        .build();
                Request request = new Request.Builder()
                        .url(url)
                        .post(req)
                        .build();

                OkHttpClient client = new OkHttpClient();
                Response response = client.newCall(request).execute();
                return_response= response.body().string();
            }
        }
        catch (Exception e)
        {
            return "";
        }
        return return_response;
    }
    @Override
    protected void onPostExecute(String result) {
        try{
            delegate.processFinished(result);
        }
        catch(Exception e) {
        }

    }
}
