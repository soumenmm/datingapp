package com.dating.app.connection;

import android.os.AsyncTask;
import android.util.Log;

import com.dating.app.mycallback.AsyncResponse;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Bishnu on 30-07-2018.
 */
public class PutRequestToServer extends AsyncTask<String,String,String> {

    public String request_json;
    public AsyncResponse delegate=null;
    public PutRequestToServer(String request_json) {
        this.request_json = request_json;
    }
    @Override
    protected String doInBackground(String... urls) {
        String return_response = "";
        try {
            for (String url : urls) {
                OkHttpClient client = new OkHttpClient();
                MediaType mediaType = MediaType.parse("application/json");
                RequestBody body = RequestBody.create(mediaType, request_json);
                Request request = new Request.Builder()
                        .url(url)
                        .put(body)
                        .addHeader("content-type", "application/json")
                        .addHeader("cache-control", "no-cache")
                        .build();


                Response response = client.newCall(request).execute();
                return response.body().string();
            }
        }
        catch (Exception e)
        {
            return "";
        }
        return return_response;
    }
    @Override
    protected void onPostExecute(String result) {
        try{
                delegate.processFinished(result);
            }
        catch(Exception e) {
            Log.d("error",""+e);
        }

    }
}
