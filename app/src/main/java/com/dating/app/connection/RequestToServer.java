package com.dating.app.connection;

import android.os.AsyncTask;


import com.dating.app.mycallback.AsyncResponse;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Bishnu on 30-07-2018.
 */
public class RequestToServer extends AsyncTask<String,String,String> {

    public AsyncResponse delegate=null;

    public RequestToServer() {

    }

    @Override
    protected String doInBackground(String... urls) {
        String return_response = "";
        try {
            for (String url : urls) {
                OkHttpClient client = new OkHttpClient();
                //client.setConnectTimeout(15, TimeUnit.SECONDS); // connect timeout
                //client.setReadTimeout(15, TimeUnit.SECONDS);    // socket timeout
                MediaType mediaType = MediaType.parse("application/octet-stream");
                Request request = new Request.Builder()
                        .url(url)
                        .addHeader("content-type", "application/json")
                        .addHeader("cache-control", "no-cache")
                        .build();

                Response response = client.newCall(request).execute();
                return_response= response.body().string();
            }
        }
        catch (Exception e)
        {
            return "";
        }
        return return_response;
    }
    @Override
    protected void onPostExecute(String result) {
        try{
            delegate.processFinished(result);
        }
        catch(Exception e) {
        }

    }
}
