package com.dating.app.connection;

import android.os.AsyncTask;

import com.dating.app.model.signup.SignUpModel;
import com.dating.app.mycallback.AsyncResponse;
import com.dating.app.utils.SharedKeys;
import com.dating.app.utils.SharedPreferenceHelper;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Bishnu on 30-07-2018.
 */
public class SignUpToServer extends AsyncTask<String,String,String> {

    public AsyncResponse delegate=null;
    private String fileName="";
    private File file;
    private SignUpModel signUpModel;

    public SignUpToServer(String fileName, File file,SignUpModel signUpModel) {
        this.file=file;
        this.fileName=fileName;
        this.signUpModel=signUpModel;
    }

    @Override
    protected String doInBackground(String... urls) {
        String return_response = "";
        try {
            for (String url : urls) {
                final MediaType MEDIA_TYPE_PNG = MediaType.parse("image/png");
                RequestBody req = new MultipartBody.Builder()
                        .setType(MultipartBody.FORM)
                        .addFormDataPart("id", signUpModel.getId())
                        .addFormDataPart("username", signUpModel.getEmail())
                        .addFormDataPart("password", signUpModel.getPassword())
                        .addFormDataPart("latitude", signUpModel.getLati())
                        .addFormDataPart("longitude", signUpModel.getLongi())
                        .addFormDataPart("age", signUpModel.getAge())
                        .addFormDataPart("phone_no", signUpModel.getPhoneNo())
                        .addFormDataPart("otp", signUpModel.getOTP())
                        .addFormDataPart("deviceid",signUpModel.getDeviceId())
                        .addFormDataPart("profile_pic",fileName, RequestBody.create(MEDIA_TYPE_PNG,file))
                        .build();
                Request request = new Request.Builder()
                        .url(url)
                        .put(req)
                        .build();

                OkHttpClient client = new OkHttpClient();
                Response response = client.newCall(request).execute();
                return_response= response.body().string();
            }
        }
        catch (Exception e)
        {
            return "";
        }
        return return_response;
    }
    @Override
    protected void onPostExecute(String result) {
        try{
            delegate.processFinished(result);
        }
        catch(Exception e) {
        }

    }
}
