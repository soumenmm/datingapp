package com.dating.app.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.dating.app.R;
import com.dating.app.adapter.GridWishListAdapter;
import com.dating.app.adapter.SwipeCardAdapterbkp;
import com.dating.app.connection.GetRequestToServer;
import com.dating.app.model.userlist.DataItem;
import com.dating.app.mycallback.AsyncResponse;
import com.dating.app.utils.Constants;
import com.dating.app.utils.GridSpacingItemDecoration;
import com.dating.app.utils.SharedKeys;
import com.dating.app.utils.SharedPreferenceHelper;
import com.dating.app.utils.Utils;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class WishListGridFragment extends Fragment {


    private RecyclerView recList;
    ArrayList<DataItem> userList = new ArrayList<DataItem>();
    private GridWishListAdapter wishListAdapter;
    private TextView txtEmpty;
    public WishListGridFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_wishlistgrid, container, false);
        recList = (RecyclerView) view.findViewById(R.id.recList);
        txtEmpty = (TextView) view.findViewById(R.id.txtEmpty);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 2);
        recList.setLayoutManager(mLayoutManager);
        recList.addItemDecoration(new GridSpacingItemDecoration(2, Utils.dpToPx(5,getActivity()), true));
        recList.setItemAnimator(new DefaultItemAnimator());

        // parseList();
        getUserList();
        return view;
    }

    void getUserList() {
        GetRequestToServer getRequestToServer = new GetRequestToServer();
        getRequestToServer.delegate = userAsyncResponse;
        // getRequestToServer.execute(Constants.myWishList+"/"+SharedPreferenceHelper.get(SharedKeys.userId));
        getRequestToServer.execute(Constants.myWishList+"/"+SharedPreferenceHelper.get(SharedKeys.userId));
    }

    AsyncResponse userAsyncResponse = new AsyncResponse() {
        @Override
        public void processFinished(String data) {
            if (data.length() > 20) {
                try {
                    JSONObject jsonObject= new JSONObject(data);
                    JSONObject mydata = jsonObject.getJSONObject("data");
                    Gson gson = new Gson();
                    DataItem[] dataItems = gson.fromJson(mydata.getJSONArray("userwish").toString(), DataItem[].class);
                    for(int i=0;i<dataItems.length;i++)
                        userList.add(dataItems[i]);
                    wishListAdapter = new GridWishListAdapter(getActivity(),userList);
                    recList.setAdapter(wishListAdapter);
                } catch (Exception e) {
                    Toast.makeText(getActivity(),""+e,Toast.LENGTH_LONG).show();
                }
            }else {
                txtEmpty.setVisibility(View.VISIBLE);
                recList.setVisibility(View.GONE);
            }
        }
    };
}
