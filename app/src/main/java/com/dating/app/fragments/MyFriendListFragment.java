package com.dating.app.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dating.app.R;
import com.dating.app.adapter.FriendListAdapter;
import com.dating.app.connection.RequestToServer;
import com.dating.app.model.friendlist.FriendListResponse;
import com.dating.app.mycallback.AsyncResponse;
import com.dating.app.utils.Constants;
import com.dating.app.utils.GridSpacingItemDecoration;
import com.dating.app.utils.SharedKeys;
import com.dating.app.utils.SharedPreferenceHelper;
import com.dating.app.utils.Utils;
import com.google.gson.Gson;


public class MyFriendListFragment extends Fragment {

    private FriendListAdapter adapter;
    private FriendListResponse friendListResponse =null;
    private RecyclerView recList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_myfriendlist, container, false);
         recList = (RecyclerView) view.findViewById(R.id.recList);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 2);
        recList.setLayoutManager(mLayoutManager);
        recList.addItemDecoration(new GridSpacingItemDecoration(2, Utils.dpToPx(5,getActivity()), true));
        recList.setItemAnimator(new DefaultItemAnimator());

       // parseList();
        apiCall();
        return view;
    }

    void apiCall(){
        RequestToServer requestToServer = new RequestToServer();
        requestToServer.delegate = asyncResponse;
        requestToServer.execute(Constants.myFriends+"/"+SharedPreferenceHelper.get(SharedKeys.userId));
    }

    AsyncResponse asyncResponse = new AsyncResponse() {
        @Override
        public void processFinished(String data) {
                if(data.length()>20) {
                    SharedPreferenceHelper.save(SharedKeys.myFriends, data);
                    parseList();
                }

        }
    };

    void parseList(){
        Gson gson = new Gson();
        String data = SharedPreferenceHelper.get(SharedKeys.myFriends);
        try {
            if(data.length()>20) {
                friendListResponse = gson.fromJson(data, FriendListResponse.class);
                adapter = new FriendListAdapter(getActivity(),friendListResponse);
                recList.setAdapter(adapter);

            }
        }catch (Exception e){}

    }


}
