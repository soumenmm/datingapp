package com.dating.app.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.dating.app.R;
import com.dating.app.adapter.SwipeCardAdapterbkp;
import com.dating.app.connection.PostRequestToServer;
import com.dating.app.connection.PutRequestToServer;
import com.dating.app.model.userlist.DataItem;
import com.dating.app.model.userlist.UserListResponse;
import com.dating.app.mycallback.AsyncResponse;
import com.dating.app.utils.Constants;
import com.dating.app.utils.ServerJson;
import com.dating.app.utils.SharedKeys;
import com.dating.app.utils.SharedPreferenceHelper;
import com.google.gson.Gson;
import com.skyfishjy.library.RippleBackground;
import com.yuyakaido.android.cardstackview.CardStackLayoutManager;
import com.yuyakaido.android.cardstackview.CardStackListener;
import com.yuyakaido.android.cardstackview.CardStackView;
import com.yuyakaido.android.cardstackview.Direction;
import com.yuyakaido.android.cardstackview.RewindAnimationSetting;
import com.yuyakaido.android.cardstackview.StackFrom;
import com.yuyakaido.android.cardstackview.SwipeAnimationSetting;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class UserListFragment extends Fragment {

    private CardStackView cardStackView;
    ArrayList<DataItem> userList = new ArrayList<DataItem>();
    private SwipeCardAdapterbkp swipeCardAdapter;
    Animation animation;
    private CardStackLayoutManager swipeManager;
    private ImageButton imgRedirect, imgCancel, imgSuperLike, imgLike, imgWishlist;
    private RippleBackground fillRipple;
    private RelativeLayout relData;

    public UserListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_userlist, container, false);

        initUI(view);
        getUserList();
        //  reload();

        return view;
    }

    void initUI(View view) {
        animation = AnimationUtils.loadAnimation(getActivity(), R.anim.fab_scale_up);
        cardStackView = (CardStackView) view.findViewById(R.id.swipeView);
        imgRedirect = (ImageButton) view.findViewById(R.id.imgRedirect);
        imgCancel = (ImageButton) view.findViewById(R.id.imgCancel);
        imgSuperLike = (ImageButton) view.findViewById(R.id.imgSuperLike);
        imgLike = (ImageButton) view.findViewById(R.id.imgLike);
        imgWishlist = (ImageButton) view.findViewById(R.id.imgWishlist);
        fillRipple = (RippleBackground) view.findViewById(R.id.fillRipple);
        relData = (RelativeLayout) view.findViewById(R.id.relData);

        swipeManager = new CardStackLayoutManager(getActivity(), cardStackListener);
        cardStackView.setLayoutManager(swipeManager);
        List<Direction> directions = Arrays.asList(Direction.Left, Direction.Right, Direction.Top,Direction.Bottom);
        swipeManager.setDirections(directions);
        swipeManager.setStackFrom(StackFrom.None);
        swipeManager.setVisibleCount(3);
        swipeManager.setTranslationInterval(8.0f);
        swipeManager.setScaleInterval(0.95f);
        swipeManager.setSwipeThreshold(0.3f);
        swipeManager.setMaxDegree(20.0f);
        swipeManager.setCanScrollHorizontal(true);
        swipeManager.setCanScrollVertical(true);
        imgRedirect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgRedirect.startAnimation(animation);
                RewindAnimationSetting setting = new RewindAnimationSetting.Builder()
                        .setDirection(Direction.Bottom)
                        .setDuration(600)
                        .setInterpolator(new DecelerateInterpolator())
                        .build();
                swipeManager.setRewindAnimationSetting(setting);
                cardStackView.rewind();
            }
        });
        imgCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgCancel.startAnimation(animation);
                SwipeAnimationSetting setting = new SwipeAnimationSetting.Builder()
                        .setDirection(Direction.Left)
                        .setDuration(600)
                        .setInterpolator(new AccelerateInterpolator())
                        .build();
                swipeManager.setSwipeAnimationSetting(setting);
                cardStackView.swipe();
            }
        });
        imgLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SwipeAnimationSetting setting = new SwipeAnimationSetting.Builder()
                        .setDirection(Direction.Right)
                        .setDuration(600)
                        .setInterpolator(new AccelerateInterpolator())
                        .build();
                swipeManager.setSwipeAnimationSetting(setting);
                cardStackView.swipe();
                imgLike.startAnimation(animation);
                try {
                    apiCallLike("" + userList.get(swipeManager.getTopPosition()), "0");
                } catch (Exception e) {
                    relData.setVisibility(View.GONE);
                    fillRipple.setVisibility(View.VISIBLE);
                    fillRipple.startRippleAnimation();
                }

            }
        });
        imgWishlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SwipeAnimationSetting setting = new SwipeAnimationSetting.Builder()
                        .setDirection(Direction.Bottom)
                        .setDuration(600)
                        .setInterpolator(new AccelerateInterpolator())
                        .build();
                swipeManager.setSwipeAnimationSetting(setting);
                cardStackView.swipe();
                imgWishlist.startAnimation(animation);
                try {
                    apiCallWishList("" + userList.get(swipeManager.getTopPosition()).getId());
                } catch (Exception e) {
                    relData.setVisibility(View.GONE);
                    fillRipple.setVisibility(View.VISIBLE);
                    fillRipple.startRippleAnimation();
                }

            }
        });

        imgSuperLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SwipeAnimationSetting setting = new SwipeAnimationSetting.Builder()
                        .setDirection(Direction.Top)
                        .setDuration(600)
                        .setInterpolator(new AccelerateInterpolator())
                        .build();
                swipeManager.setSwipeAnimationSetting(setting);
                cardStackView.swipe();
                imgSuperLike.startAnimation(animation);
                try {
                    apiCallLike("" + userList.get(swipeManager.getTopPosition()), "1");
                } catch (Exception e) {
                    relData.setVisibility(View.GONE);
                    fillRipple.setVisibility(View.VISIBLE);
                    fillRipple.startRippleAnimation();
                }

            }
        });

    }


    private void paginate() {
        // adapter.addAll(userList);
        // adapter.notifyDataSetChanged();
    }


    void getUserList() {
        LocationManager lm = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        @SuppressLint("MissingPermission")
        Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        //String json = ServerJson.getInstance().jsonUserList("" + location.getLongitude(), "" + location.getLatitude());
        String json = ServerJson.getInstance().jsonUserList("", "");
        PostRequestToServer postRequestToServer = new PostRequestToServer(json);
        postRequestToServer.delegate = userAsyncResponse;
        postRequestToServer.execute(Constants.userListing);
    }

    AsyncResponse userAsyncResponse = new AsyncResponse() {
        @Override
        public void processFinished(String data) {
            if (data.length() > 20) {
                try {
                    Gson gson = new Gson();
                    UserListResponse userListResponses = gson.fromJson(data, UserListResponse.class);
                    for (int i = 0; i < userListResponses.getData().size(); i++)
                        userList.add(userListResponses.getData().get(i));
                    swipeCardAdapter = new SwipeCardAdapterbkp(getActivity(), userList);
                    //swipeCardAdapter.addAll(userList);
                    cardStackView.setAdapter(swipeCardAdapter);
                } catch (Exception e) {
                    Toast.makeText(getActivity(), "" + e, Toast.LENGTH_LONG).show();
                }
            } else {
                relData.setVisibility(View.GONE);
                fillRipple.setVisibility(View.VISIBLE);
                fillRipple.startRippleAnimation();
            }
        }
    };

    void apiCallLike(String id, String superLike) {
        String json = ServerJson.getInstance().jsonLike(id, superLike);
        PostRequestToServer postRequestToServer = new PostRequestToServer(json);
        postRequestToServer.delegate = likeAsyncResponse;
        postRequestToServer.execute(Constants.like);
    }

    void apiCallWishList(String id) {
        String json = ServerJson.getInstance().jsonWishList(id);
        PutRequestToServer postRequestToServer = new PutRequestToServer(json);
        postRequestToServer.delegate = addWishAsyncResponse;
        postRequestToServer.execute(Constants.addToWishList + "/" + SharedPreferenceHelper.get(SharedKeys.userId));
    }

    AsyncResponse addWishAsyncResponse = new AsyncResponse() {
        @Override
        public void processFinished(String data) {
            Log.d("CardStackView", "WISHLIST: " + data);

        }
    };

    AsyncResponse likeAsyncResponse = new AsyncResponse() {
        @Override
        public void processFinished(String data) {
            Log.d("CardStackView", "LIKE: " + data);

        }
    };

    CardStackListener cardStackListener = new CardStackListener() {
        @Override
        public void onCardDragging(Direction direction, float ratio) {

        }

        @Override
        public void onCardSwiped(Direction direction) {
            try{
            if (direction.toString().equalsIgnoreCase("Left")) {
                apiCallLike("" + userList.get(swipeManager.getTopPosition()), "0");
            } else if (direction.toString().equalsIgnoreCase("TOP")) {
                apiCallLike("" + userList.get(swipeManager.getTopPosition()), "1");
                // apiCallWishList(""+userList.get(cardStackView.getTop()).getId());
            }else if (direction.toString().equalsIgnoreCase("BOTTOM")) {
                apiCallWishList("" + userList.get(swipeManager.getTopPosition()).getId());
                // apiCallWishList(""+userList.get(cardStackView.getTop()).getId());
            }
            }catch (Exception e){
                relData.setVisibility(View.GONE);
                fillRipple.setVisibility(View.VISIBLE);
                fillRipple.startRippleAnimation();
            }


        }

        @Override
        public void onCardRewound() {

        }

        @Override
        public void onCardCanceled() {

        }

        @Override
        public void onCardAppeared(View view, int position) {

        }

        @Override
        public void onCardDisappeared(View view, int position) {

        }
    };

}
