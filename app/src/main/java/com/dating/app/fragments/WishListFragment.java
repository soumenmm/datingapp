package com.dating.app.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dating.app.R;
import com.dating.app.adapter.SwipeCardAdapter;
import com.dating.app.adapter.SwipeCardAdapterbkp;
import com.dating.app.connection.GetRequestToServer;
import com.dating.app.connection.PostRequestToServer;
import com.dating.app.connection.PutRequestToServer;
import com.dating.app.model.userlist.DataItem;
import com.dating.app.mycallback.AsyncResponse;
import com.dating.app.utils.Constants;
import com.dating.app.utils.ServerJson;
import com.dating.app.utils.SharedKeys;
import com.dating.app.utils.SharedPreferenceHelper;
import com.google.gson.Gson;
import com.vlonjatg.progressactivity.ProgressRelativeLayout;
import com.yuyakaido.android.cardstackview.CardStackLayoutManager;
import com.yuyakaido.android.cardstackview.CardStackListener;
import com.yuyakaido.android.cardstackview.CardStackView;
import com.yuyakaido.android.cardstackview.Direction;
import com.yuyakaido.android.cardstackview.RewindAnimationSetting;
import com.yuyakaido.android.cardstackview.StackFrom;
import com.yuyakaido.android.cardstackview.SwipeAnimationSetting;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class WishListFragment extends Fragment {

    private CardStackView cardStackView;
    ArrayList<DataItem> userList = new ArrayList<DataItem>();
    private SwipeCardAdapterbkp swipeCardAdapter;
    Animation animation;
    private CardStackLayoutManager swipeManager;
    private ImageButton imgRedirect,imgCancel,imgSuperLike,imgLike,imgWishlist;
    private RelativeLayout relData;
    private TextView txtEmpty;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_wish_list, container, false);
        initUI(view);

        getUserList();
        //  reload();

        return view;
    }


    void initUI(View view) {
        animation = AnimationUtils.loadAnimation(getActivity(), R.anim.fab_scale_up);
        cardStackView = (CardStackView) view.findViewById(R.id.swipeView);
        imgRedirect = (ImageButton) view.findViewById(R.id.imgRedirect);
        imgCancel = (ImageButton) view.findViewById(R.id.imgCancel);
        imgSuperLike = (ImageButton) view.findViewById(R.id.imgSuperLike);
        imgLike = (ImageButton) view.findViewById(R.id.imgLike);
        imgWishlist = (ImageButton) view.findViewById(R.id.imgWishlist);
        relData = (RelativeLayout) view.findViewById(R.id.relData);
        txtEmpty = (TextView) view.findViewById(R.id.txtEmpty);

        swipeManager = new CardStackLayoutManager(getActivity(),cardStackListener);
        cardStackView.setLayoutManager(swipeManager);
        List<Direction> directions = Arrays.asList(Direction.Left, Direction.Right,Direction.Top);
        swipeManager.setDirections(directions);
        swipeManager.setStackFrom(StackFrom.None);
        swipeManager.setVisibleCount(3);
        swipeManager.setTranslationInterval(8.0f);
        swipeManager.setScaleInterval(0.95f);
        swipeManager.setSwipeThreshold(0.3f);
        swipeManager.setMaxDegree(20.0f);
        swipeManager.setCanScrollHorizontal(true);
        swipeManager.setCanScrollVertical(true);
        imgRedirect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgRedirect.startAnimation(animation);
                RewindAnimationSetting setting = new RewindAnimationSetting.Builder()
                        .setDirection(Direction.Bottom)
                        .setDuration(600)
                        .setInterpolator(new DecelerateInterpolator())
                        .build();
                swipeManager.setRewindAnimationSetting(setting);
                cardStackView.rewind();
            }
        });
        imgCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgCancel.startAnimation(animation);
                SwipeAnimationSetting setting = new SwipeAnimationSetting.Builder()
                        .setDirection(Direction.Left)
                        .setDuration(600)
                        .setInterpolator(new AccelerateInterpolator())
                        .build();
                swipeManager.setSwipeAnimationSetting(setting);
                cardStackView.swipe();
            }
        });
        imgLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SwipeAnimationSetting setting = new SwipeAnimationSetting.Builder()
                        .setDirection(Direction.Right)
                        .setDuration(600)
                        .setInterpolator(new AccelerateInterpolator())
                        .build();
                swipeManager.setSwipeAnimationSetting(setting);
                cardStackView.swipe();
                imgLike.startAnimation(animation);
                apiCallLike(""+userList.get(swipeManager.getTopPosition()),"0");

            }
        });
        imgWishlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SwipeAnimationSetting setting = new SwipeAnimationSetting.Builder()
                        .setDirection(Direction.Right)
                        .setDuration(600)
                        .setInterpolator(new AccelerateInterpolator())
                        .build();
                swipeManager.setSwipeAnimationSetting(setting);
                cardStackView.swipe();
                imgWishlist.startAnimation(animation);
                apiCallWishList(""+userList.get(swipeManager.getTopPosition()).getId());

            }
        });

        imgSuperLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SwipeAnimationSetting setting = new SwipeAnimationSetting.Builder()
                        .setDirection(Direction.Top)
                        .setDuration(600)
                        .setInterpolator(new AccelerateInterpolator())
                        .build();
                swipeManager.setSwipeAnimationSetting(setting);
                cardStackView.swipe();
                imgSuperLike.startAnimation(animation);
                apiCallLike(""+userList.get(swipeManager.getTopPosition()),"1");

            }
        });

    }




    private void paginate() {
       // cardStackView.setPaginationReserved();
        // adapter.addAll(userList);
      //  adapter.notifyDataSetChanged();
    }


    void getUserList() {
        GetRequestToServer getRequestToServer = new GetRequestToServer();
        getRequestToServer.delegate = userAsyncResponse;
       // getRequestToServer.execute(Constants.myWishList+"/"+SharedPreferenceHelper.get(SharedKeys.userId));
        getRequestToServer.execute(Constants.myWishList+"/"+SharedPreferenceHelper.get(SharedKeys.userId));
    }

    AsyncResponse userAsyncResponse = new AsyncResponse() {
        @Override
        public void processFinished(String data) {
            if (data.length() > 20) {
                try {
                    JSONObject jsonObject= new JSONObject(data);
                    JSONObject mydata = jsonObject.getJSONObject("data");
                    Gson gson = new Gson();
                    DataItem[] dataItems = gson.fromJson(mydata.getJSONArray("userwish").toString(), DataItem[].class);
                    for(int i=0;i<dataItems.length;i++)
                        userList.add(dataItems[i]);
                    swipeCardAdapter = new SwipeCardAdapterbkp(getActivity(),userList);
                    cardStackView.setAdapter(swipeCardAdapter);
                } catch (Exception e) {
                    Toast.makeText(getActivity(),""+e,Toast.LENGTH_LONG).show();
                }
            }else {
                txtEmpty.setVisibility(View.VISIBLE);
                relData.setVisibility(View.GONE);
            }
        }
    };

    void apiCallLike(String id,String superLike){
        String json = ServerJson.getInstance().jsonLike(id, superLike);
        PostRequestToServer postRequestToServer = new PostRequestToServer(json);
        postRequestToServer.delegate = likeAsyncResponse;
        postRequestToServer.execute(Constants.like);
    }

    void apiCallWishList(String id){
        String json = ServerJson.getInstance().jsonWishList(id);
        PutRequestToServer postRequestToServer = new PutRequestToServer(json);
        postRequestToServer.delegate = addWishAsyncResponse;
        postRequestToServer.execute(Constants.addToWishList+"/"+SharedPreferenceHelper.get(SharedKeys.userId));
    }

    AsyncResponse addWishAsyncResponse = new AsyncResponse() {
        @Override
        public void processFinished(String data) {
            Log.d("CardStackView", "WISHLIST: " + data);

        }
    };

    AsyncResponse likeAsyncResponse = new AsyncResponse() {
        @Override
        public void processFinished(String data) {
            Log.d("CardStackView", "LIKE: " + data);

        }
    };

    CardStackListener cardStackListener = new CardStackListener() {
        @Override
        public void onCardDragging(Direction direction, float ratio) {

        }

        @Override
        public void onCardSwiped(Direction direction) {
            try {
                if (direction.toString().equalsIgnoreCase("Left")) {
                    apiCallLike("" + userList.get(swipeManager.getTopPosition()), "0");
                } else if (direction.toString().equalsIgnoreCase("TOP")) {
                    apiCallLike("" + userList.get(swipeManager.getTopPosition()), "1");
                    // apiCallWishList(""+userList.get(cardStackView.getTop()).getId());
                }
            }catch (Exception e){
                txtEmpty.setVisibility(View.VISIBLE);
                relData.setVisibility(View.GONE);
            }

        }

        @Override
        public void onCardRewound() {

        }

        @Override
        public void onCardCanceled() {

        }

        @Override
        public void onCardAppeared(View view, int position) {

        }

        @Override
        public void onCardDisappeared(View view, int position) {

        }
    };

}
