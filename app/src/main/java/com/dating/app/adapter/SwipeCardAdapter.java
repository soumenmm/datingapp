package com.dating.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.dating.app.R;
import com.dating.app.model.userlist.DataItem;
import com.dating.app.utils.Constants;

import java.util.List;

public class SwipeCardAdapter extends ArrayAdapter<DataItem> {

    List<DataItem> userList;
    private int counter=0;
    private int totalCount=0;
    private DataItem user;
    private Context context;
    public SwipeCardAdapter(Context context, List<DataItem> userList) {
        super(context, 0);
        this.userList =userList;
        this.context =context;
    }

    @Override
    public int getCount() {
        return userList.size();
    }

    @Override
    public View getView(int position, View contentView, ViewGroup parent) {
        final ViewHolder holder;

        if (contentView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            contentView = inflater.inflate(R.layout.item_swipecard, parent, false);
            holder = new ViewHolder(contentView);
            contentView.setTag(holder);
        } else {
            holder = (ViewHolder) contentView.getTag();
        }

        user = userList.get(position);

        if(user.getFullname()!=null)
        holder.name.setText(user.getFullname());

        if(user.getCity()!=null)
            holder.city.setText(""+user.getCity());
        try {
            totalCount = user.getUserprofileimages().size();
            if (user.getUserprofileimages().get(counter) != null)
                Glide.with(getContext())
                        .load(Constants.rootURL + user.getUserprofileimages().get(counter))
                        .apply(new RequestOptions().placeholder(R.drawable.image_placeholder).error(R.drawable.image_placeholder))
                        .into(holder.image);
    /*    holder.reverse.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Toast.makeText(context,"counter"+counter+"\n"+totalCount,Toast.LENGTH_LONG).show();
                if(counter<=totalCount-1)
                    return true;
                counter=counter-1;
                if (user.getUserprofileimages().get(counter).getUserProfilePics() != null)
                    Glide.with(getContext()).load(Constants.rootURL + user.getUserprofileimages().get(counter).getUserProfilePics()).into(holder.image);
                return false;

            }
        });

            holder.next.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    Toast.makeText(context,"counter"+counter+"\n"+totalCount,Toast.LENGTH_LONG).show();
                    if(counter==totalCount-1)
                        return true;
                    counter=counter+1;
                    if (user.getUserprofileimages().get(counter).getUserProfilePics() != null)
                        Glide.with(getContext()).load(Constants.rootURL + user.getUserprofileimages().get(counter).getUserProfilePics()).into(holder.image);

                    return false;
                }

            });*/
        }catch (Exception e){}
        return contentView;
    }

    private static class ViewHolder {
        public TextView name;
        public TextView city;
        public ImageView image;
       // public View reverse;
       // public View next;

        public ViewHolder(View view) {
            this.name = (TextView) view.findViewById(R.id.item_name);
            this.city = (TextView) view.findViewById(R.id.item_city);
            this.image = (ImageView) view.findViewById(R.id.item_image);
           //this.reverse =  view.findViewById(R.id.reverse);
          //  this.next =  view.findViewById(R.id.next);
        }
    }

}

