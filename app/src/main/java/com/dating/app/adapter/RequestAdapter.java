package com.dating.app.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.dating.app.R;
import com.dating.app.activity.UserDetailActivity;
import com.dating.app.model.friendlist.DataItem;
import com.dating.app.model.friendlist.Liked;
import com.dating.app.model.userdetails.UserDetailsModel;
import com.dating.app.utils.Constants;
import com.paytm.pgsdk.Log;

import java.util.List;

public class RequestAdapter extends RecyclerView.Adapter<RequestAdapter.ViewHolder> {

    List<DataItem> userList;
    private int counter=0;
    private int totalCount=0;
    private Liked user;
    private Context context;
    public RequestAdapter(Context context, List<DataItem> userList) {
        this.userList =userList;
        this.context =context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_swipecard, viewGroup, false);

        return new RequestAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        user = userList.get(position).getLiked();

        if(user.getFullname()!=null)
            viewHolder.name.setText(user.getFullname());

        if(user.getCity()!=null)
            viewHolder.city.setText(""+user.getCity());

        viewHolder.txtSuperLike.setText("SKIP PROFILE");
        try {
            totalCount = user.getUserprofileimages().size();
            if (user.getUserprofileimages() != null) {
                Glide.with(context)
                        .load(Constants.rootURL +user.getUserprofileimages().get(0).getUserProfilePics())
                        //.apply(new RequestOptions().placeholder(R.drawable.image_placeholder).error(R.drawable.image_placeholder))
                        .load(Constants.rootURL + user.getUserprofileimages().get(0).getUserProfilePics())
                        .apply(new RequestOptions().placeholder(R.drawable.image_placeholder).error(R.drawable.image_placeholder))
                        .into(viewHolder.image);
            }
    /*    holder.reverse.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Toast.makeText(context,"counter"+counter+"\n"+totalCount,Toast.LENGTH_LONG).show();
                if(counter<=totalCount-1)
                    return true;
                counter=counter-1;
                if (user.getUserprofileimages().get(counter).getUserProfilePics() != null)
                    Glide.with(getContext()).load(Constants.rootURL + user.getUserprofileimages().get(counter).getUserProfilePics()).into(holder.image);
                return false;

            }
        });

            holder.next.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    Toast.makeText(context,"counter"+counter+"\n"+totalCount,Toast.LENGTH_LONG).show();
                    if(counter==totalCount-1)
                        return true;
                    counter=counter+1;
                    if (user.getUserprofileimages().get(counter).getUserProfilePics() != null)
                        Glide.with(getContext()).load(Constants.rootURL + user.getUserprofileimages().get(counter).getUserProfilePics()).into(holder.image);

                    return false;
                }

            });*/
    viewHolder.imgInfo.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            UserDetailsModel userDetailsModel = new UserDetailsModel();
            if(user.getAboutYou()!=null)
                userDetailsModel.setStrAbout(user.getAboutYou());
            if(user.getAge()!=0)
                userDetailsModel.setStrAge(""+user.getAge());
            if(user.getEmail()!=null)
                userDetailsModel.setStrEmail(user.getEmail());
            if(user.getPhoneNo()!=null)
                userDetailsModel.setStrPhoneVerification("");
            if(user.getBodyType()!=0)
                userDetailsModel.setStrBodyType(""+user.getBodyType());
            if(user.getMaritalStatus()!=null)
                userDetailsModel.setStrMaritalStatus(""+user.getMaritalStatus());
            if(user.getGender()!=null)
                userDetailsModel.setStrGender(""+user.getGender());
            if(user.getCurrentAddress()!=null)
                userDetailsModel.setStrCurrentAddress(""+user.getCurrentAddress());
            if(user.getPermanentAddress()!=null)
                userDetailsModel.setStrPermanentAddress(""+user.getPermanentAddress());
            if(user.getPinCode()!=null)
                userDetailsModel.setStrPincode(""+user.getPinCode());
            if(user.getWeight()!=null)
                userDetailsModel.setStrWeight(""+user.getWeight());
            if(user.getHeight()!=null)
                userDetailsModel.setStrheight(""+user.getHeight());
            if(user.getIncome()!=null)
                userDetailsModel.setStrIncome(""+user.getIncome());
            for(int i = 0;i<user.getUserprofileimages().size();i++){
                if(i==0) {
                    userDetailsModel.setImageUrl1(user.getUserprofileimages().get(i).getUserProfilePics());
                    userDetailsModel.setPicCount(i + 1);
                }if(i==1) {
                    userDetailsModel.setImageUrl2(user.getUserprofileimages().get(i).getUserProfilePics());
                    userDetailsModel.setPicCount(i + 1);
                }if(i==2) {
                    userDetailsModel.setImageUrl3(user.getUserprofileimages().get(i).getUserProfilePics());
                    userDetailsModel.setPicCount(i + 1);
                }if(i==3) {
                    userDetailsModel.setImageUrl4(user.getUserprofileimages().get(i).getUserProfilePics());
                    userDetailsModel.setPicCount(i + 1);
                }if(i==4) {
                    userDetailsModel.setImageUrl5(user.getUserprofileimages().get(i).getUserProfilePics());
                    userDetailsModel.setPicCount(i + 1);
                }if(i==5) {
                    userDetailsModel.setImageUrl6(user.getUserprofileimages().get(i).getUserProfilePics());
                    userDetailsModel.setPicCount(i + 1);
                }
            }
            Intent intent = new Intent(context,UserDetailActivity.class);
            intent.putExtra("UserModel",userDetailsModel);
            context.startActivity(intent);
        }
    });
        }catch (Exception e){

            Log.d("error",""+e);
        }
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView name;
        public TextView city;
        public ImageView image;
        public ImageView imgInfo;
        public TextView txtSuperLike;
        //public View reverse;
       // public View next;

        public ViewHolder(View view) {
            super(view);
            this.name = (TextView) view.findViewById(R.id.item_name);
            this.city = (TextView) view.findViewById(R.id.item_city);
            this.image = (ImageView) view.findViewById(R.id.item_image);
            this.imgInfo = (ImageView) view.findViewById(R.id.imgInfo);
            this.txtSuperLike = (TextView) view.findViewById(R.id.txtSuperLike);
           //this.reverse =  view.findViewById(R.id.reverse);
            //this.next =  view.findViewById(R.id.next);
        }
    }

}

