package com.dating.app.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.dating.app.R;
import com.dating.app.model.userdetails.UserDetailsModel;
import com.dating.app.model.userlist.DataItem;
import com.dating.app.utils.Constants;

import java.util.ArrayList;

import static com.paytm.pgsdk.easypay.manager.PaytmAssist.getContext;

public class ImagePager extends PagerAdapter {

    Context mContext;
    LayoutInflater mLayoutInflater;
    private UserDetailsModel item;

    public ImagePager(Context context,UserDetailsModel item) {
        mContext = context;
        this.item =item;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return item.getPicCount();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((FrameLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.item_image, container, false);

        ImageView imageView = (ImageView) itemView.findViewById(R.id.imageView);
        String imgUL="";
        if (position==0)
            imgUL =item.getImageUrl1();
        if (position==1)
            imgUL =item.getImageUrl2();
        if (position==2)
            imgUL =item.getImageUrl3();
        if (position==3)
            imgUL =item.getImageUrl4();
        if (position==4)
            imgUL =item.getImageUrl5();
        if (position==5)
            imgUL =item.getImageUrl6();


        Glide.with(mContext)
                .load(Constants.rootURL + imgUL)
                .apply(new RequestOptions().placeholder(R.drawable.image_placeholder).error(R.drawable.image_placeholder))
                .into(imageView);

        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((FrameLayout) object);
    }
}