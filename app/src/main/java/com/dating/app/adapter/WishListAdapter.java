package com.dating.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dating.app.R;
import com.dating.app.model.TouristSpot;
import com.makeramen.roundedimageview.RoundedImageView;

public class WishListAdapter extends ArrayAdapter<TouristSpot> {

    public WishListAdapter(Context context) {
        super(context, 0);
    }

    @Override
    public View getView(int position, View contentView, ViewGroup parent) {
        ViewHolder holder;

        if (contentView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            contentView = inflater.inflate(R.layout.item_swipecard, parent, false);
            holder = new ViewHolder(contentView);
            contentView.setTag(holder);
        } else {
            holder = (ViewHolder) contentView.getTag();
        }

        TouristSpot spot = getItem(position);

        holder.name.setText(spot.name);
        holder.city.setText(spot.city);
        Glide.with(getContext()).load(spot.url).into(holder.image);

        return contentView;
    }

    private static class ViewHolder {
        public TextView name;
        public TextView city;
        public RoundedImageView image;
        public ImageView imgInfo;

        public ViewHolder(View view) {
            this.name = (TextView) view.findViewById(R.id.item_name);
            this.city = (TextView) view.findViewById(R.id.item_city);
            this.image = (RoundedImageView) view.findViewById(R.id.item_image);
            this.imgInfo = (ImageView) view.findViewById(R.id.imgInfo);
        }
    }

}

