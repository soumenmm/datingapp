package com.dating.app.adapter;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.dating.app.R;
import com.dating.app.activity.UserDetailActivity;
import com.dating.app.model.friendlist.FriendListResponse;
import com.dating.app.model.friendlist.Liked;
import com.dating.app.model.userdetails.UserDetailsModel;
import com.dating.app.utils.Constants;


public class FriendListAdapter extends RecyclerView.Adapter<FriendListAdapter.MyViewHolder> {
    private Liked dataItem;
    private Context context;
    private FriendListResponse friendList;
    public FriendListAdapter(Context context,FriendListResponse friendList) {
        this.context = context;
        this.friendList = friendList;

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_userlist, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

         dataItem = friendList.getData().get(position).getLiked();
        try {
            if(dataItem.getFullname() != null)
            holder.txtName.setText(""+dataItem.getFullname());
            if(dataItem.getAge() != 0)
            //holder.age.setText("" + dataItem.getAge());
            if (!dataItem.getUserprofileimages().isEmpty()) {
                Glide.with(context)
                        .load(Constants.rootURL + dataItem.getUserprofileimages().get(0).getUserProfilePics())
                        .apply(new RequestOptions().placeholder(R.drawable.avatar).error(R.drawable.avatar))
                        .into(holder.imgProfile);

            }
        }catch (Exception e){
            Log.d("Errorr",""+e);
        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UserDetailsModel userDetailsModel = new UserDetailsModel();
                if(dataItem.getAboutYou()!=null)
                    userDetailsModel.setStrAbout(dataItem.getAboutYou());
                if(dataItem.getAge()!=0)
                    userDetailsModel.setStrAge(""+dataItem.getAge());
                if(dataItem.getEmail()!=null)
                    userDetailsModel.setStrEmail(dataItem.getEmail());
                if(dataItem.getPhoneNo()!=null)
                    userDetailsModel.setStrPhoneVerification("");
                if(dataItem.getBodyType()!=0)
                    userDetailsModel.setStrBodyType(""+dataItem.getBodyType());
                if(dataItem.getMaritalStatus()!=null)
                    userDetailsModel.setStrMaritalStatus(""+dataItem.getMaritalStatus());
                if(dataItem.getGender()!=null)
                    userDetailsModel.setStrGender(""+dataItem.getGender());
                if(dataItem.getCurrentAddress()!=null)
                    userDetailsModel.setStrCurrentAddress(""+dataItem.getCurrentAddress());
                if(dataItem.getPermanentAddress()!=null)
                    userDetailsModel.setStrPermanentAddress(""+dataItem.getPermanentAddress());
                if(dataItem.getPinCode()!=null)
                    userDetailsModel.setStrPincode(""+dataItem.getPinCode());
                if(dataItem.getWeight()!=null)
                    userDetailsModel.setStrWeight(""+dataItem.getWeight());
                if(dataItem.getHeight()!=null)
                    userDetailsModel.setStrheight(""+dataItem.getHeight());
                if(dataItem.getIncome()!=null)
                    userDetailsModel.setStrIncome(""+dataItem.getIncome());
                for(int i = 0;i<dataItem.getUserprofileimages().size();i++){
                    if(i==0) {
                        userDetailsModel.setImageUrl1(dataItem.getUserprofileimages().get(i).getUserProfilePics());
                        userDetailsModel.setPicCount(i + 1);
                    }if(i==1) {
                        userDetailsModel.setImageUrl2(dataItem.getUserprofileimages().get(i).getUserProfilePics());
                        userDetailsModel.setPicCount(i + 1);
                    }if(i==2) {
                        userDetailsModel.setImageUrl3(dataItem.getUserprofileimages().get(i).getUserProfilePics());
                        userDetailsModel.setPicCount(i + 1);
                    }if(i==3) {
                        userDetailsModel.setImageUrl4(dataItem.getUserprofileimages().get(i).getUserProfilePics());
                        userDetailsModel.setPicCount(i + 1);
                    }if(i==4) {
                        userDetailsModel.setImageUrl5(dataItem.getUserprofileimages().get(i).getUserProfilePics());
                        userDetailsModel.setPicCount(i + 1);
                    }if(i==5) {
                        userDetailsModel.setImageUrl6(dataItem.getUserprofileimages().get(i).getUserProfilePics());
                        userDetailsModel.setPicCount(i + 1);
                    }
                }
                Intent intent = new Intent(context,UserDetailActivity.class);
                intent.putExtra("UserModel",userDetailsModel);
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return friendList.getData().size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtName;
        public TextView age;
        public ImageView imgProfile;
        public Button remove;
        public Button addfriend;

        public MyViewHolder(View view) {
            super(view);
            this.txtName = (TextView) view.findViewById(R.id.txtName);
           // this.age = (TextView) view.findViewById(R.id.item_age);
            this.imgProfile = (ImageView) view.findViewById(R.id.imgProfile);
          //  this.addfriend =  view.findViewById(R.id.btn_AddFriend);
          //  this.remove =  view.findViewById(R.id.btn_Remove);

        }
    }

}
