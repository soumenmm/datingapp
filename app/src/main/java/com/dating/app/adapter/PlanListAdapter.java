package com.dating.app.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dating.app.R;
import com.dating.app.activity.plans.PlanDialog;
import com.dating.app.model.plan.PlanResponse;
import com.dating.app.model.plan.Plandetail;
import com.dating.app.mycallback.PaytmCustomResponse;

import java.util.List;

public class PlanListAdapter extends RecyclerView.Adapter<PlanListAdapter.ViewHolder> {

    private Context context;
    private PlanResponse[] planResponse;
    private List<Plandetail> planDetails;
    private PaytmCustomResponse paytmCustomResponse;
    private String walletAmount;
    public PlanListAdapter(Context context,String walletAmount, PlanResponse[] planResponse, PaytmCustomResponse paytmCustomResponse) {
        this.planResponse=planResponse;
        this.context =context;
        this.walletAmount =walletAmount;
        this.paytmCustomResponse =paytmCustomResponse;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.plan_card, viewGroup, false);

        return new PlanListAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int position) {

         planDetails = planResponse[0].getPlandetail();

         viewHolder.txtDesc.setText(planResponse[position].getAboutplan());
         viewHolder.txtPlanName.setText(planResponse[position].getName());

         viewHolder.relAbout.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 PlanDialog planDialog = new PlanDialog(context,walletAmount,planResponse[position],paytmCustomResponse);
                 planDialog.show();
             }
         });

    }

    @Override
    public int getItemCount() {
        return planResponse.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txtPlanName;
        public TextView txtDesc;
        public RelativeLayout relAbout;

        public ViewHolder(View view) {
            super(view);
            this.txtPlanName = (TextView) view.findViewById(R.id.txtPlanName);
            this.txtDesc = (TextView) view.findViewById(R.id.txtDesc);
            this.relAbout = (RelativeLayout) view.findViewById(R.id.relAbout);
        }
    }

}

