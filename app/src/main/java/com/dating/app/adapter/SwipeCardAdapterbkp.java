package com.dating.app.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.dating.app.R;
import com.dating.app.activity.UserDetailActivity;
import com.dating.app.model.userdetails.UserDetailsModel;
import com.dating.app.model.userlist.DataItem;
import com.dating.app.utils.Constants;
import com.paytm.pgsdk.Log;

import java.util.ArrayList;
import java.util.List;

import xute.storyview.StoryModel;
import xute.storyview.StoryView;

import static com.paytm.pgsdk.easypay.manager.PaytmAssist.getContext;

public class SwipeCardAdapterbkp extends RecyclerView.Adapter<SwipeCardAdapterbkp.ViewHolder> {

    List<DataItem> userList;
    private int counter=0;
    private int totalCount=0;
    private DataItem user;
    private Context context;
    public SwipeCardAdapterbkp(Context context, List<DataItem> userList) {
        this.userList =userList;
        this.context =context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_swipecard, viewGroup, false);

        return new SwipeCardAdapterbkp.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        user = userList.get(position);

        if(user.getFullname()!=null)
            viewHolder.name.setText(user.getFullname());

        if(user.getCity()!=null)
            viewHolder.city.setText(""+user.getCity());
        try {
            totalCount = user.getUserprofileimages().size();

            if (user.getUserprofileimages() != null) {
                ArrayList<StoryModel> uris = new ArrayList<>();
                for(int i=0;i<totalCount;i++) {
                    uris.add(new StoryModel(Constants.rootURL +user.getUserprofileimages().get(i).getUserProfilePics(), "", ""));
                    viewHolder.storyView.setImageUris(uris);
                }
               Glide.with(context)
                        .load(Constants.rootURL +user.getUserprofileimages().get(0).getUserProfilePics())
                        //.apply(new RequestOptions().placeholder(R.drawable.image_placeholder).error(R.drawable.image_placeholder))
                        .load(Constants.rootURL + user.getUserprofileimages().get(0).getUserProfilePics())
                        .apply(new RequestOptions().placeholder(R.drawable.image_placeholder).error(R.drawable.image_placeholder))
                        .into(viewHolder.image);
            }
    viewHolder.imgInfo.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            UserDetailsModel userDetailsModel = new UserDetailsModel();
            if(user.getAboutYou()!=null)
            userDetailsModel.setStrAbout(user.getAboutYou());
            if(user.getAge()!=null)
            userDetailsModel.setStrAge(""+user.getAge());
            if(user.getEmail()!=null)
            userDetailsModel.setStrEmail(user.getEmail());
            if(user.getPhoneVerification()!=null)
            userDetailsModel.setStrPhoneVerification(""+user.getPhoneVerification());
            if(user.getBodyType()!=null)
            userDetailsModel.setStrBodyType(""+user.getBodyType());
            if(user.getMaritalStatus()!=null)
            userDetailsModel.setStrMaritalStatus(""+user.getMaritalStatus());
            if(user.getGender()!=null)
            userDetailsModel.setStrGender(""+user.getGender());
            if(user.getCurrentAddress()!=null)
            userDetailsModel.setStrCurrentAddress(""+user.getCurrentAddress());
            if(user.getPermanentAddress()!=null)
            userDetailsModel.setStrPermanentAddress(""+user.getPermanentAddress());
            if(user.getPinCode()!=null)
            userDetailsModel.setStrPincode(""+user.getPinCode());
            if(user.getWeight()!=null)
            userDetailsModel.setStrWeight(""+user.getWeight());
            if(user.getHeight()!=null)
            userDetailsModel.setStrheight(""+user.getHeight());
            if(user.getIncome()!=null)
            userDetailsModel.setStrIncome(""+user.getIncome());
            for(int i = 0;i<user.getUserprofileimages().size();i++){
                if(i==0) {
                    userDetailsModel.setImageUrl1(user.getUserprofileimages().get(i).getUserProfilePics());
                    userDetailsModel.setPicCount(i + 1);
                }if(i==1) {
                    userDetailsModel.setImageUrl2(user.getUserprofileimages().get(i).getUserProfilePics());
                    userDetailsModel.setPicCount(i + 1);
                }if(i==2) {
                    userDetailsModel.setImageUrl3(user.getUserprofileimages().get(i).getUserProfilePics());
                    userDetailsModel.setPicCount(i + 1);
                }if(i==3) {
                    userDetailsModel.setImageUrl4(user.getUserprofileimages().get(i).getUserProfilePics());
                    userDetailsModel.setPicCount(i + 1);
                }if(i==4) {
                    userDetailsModel.setImageUrl5(user.getUserprofileimages().get(i).getUserProfilePics());
                    userDetailsModel.setPicCount(i + 1);
                }if(i==5) {
                    userDetailsModel.setImageUrl6(user.getUserprofileimages().get(i).getUserProfilePics());
                    userDetailsModel.setPicCount(i + 1);
                }
            }
            Intent intent = new Intent(context,UserDetailActivity.class);
            intent.putExtra("UserModel",userDetailsModel);
            context.startActivity(intent);
        }
    });
        }catch (Exception e){

            Log.d("error",""+e);
        }
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView name;
        public TextView city;
        public StoryView storyView;
        public ImageView image;
        public ImageView imgInfo;
        //public View reverse;
       // public View next;

        public ViewHolder(View view) {
            super(view);
            this.name = (TextView) view.findViewById(R.id.item_name);
            this.city = (TextView) view.findViewById(R.id.item_city);
            this.storyView = (StoryView) view.findViewById(R.id.storyView);
            this.image = (ImageView) view.findViewById(R.id.item_image);
            this.imgInfo = (ImageView) view.findViewById(R.id.imgInfo);
           //this.reverse =  view.findViewById(R.id.reverse);
            //this.next =  view.findViewById(R.id.next);
        }
    }

}

