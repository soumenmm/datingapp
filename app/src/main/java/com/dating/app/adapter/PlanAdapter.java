package com.dating.app.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dating.app.R;
import com.dating.app.model.plan.PlanResponse;

import java.util.ArrayList;

public class PlanAdapter extends PagerAdapter {

    private LayoutInflater layoutInflater;
    private ArrayList<PlanResponse> itemList;
    private Context context;
    private TextView txtPlanName;
    private TextView txtDisc;

    public PlanAdapter(Context context, ArrayList<PlanResponse> item) {
        itemList = item;

        this.context = context;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = layoutInflater.inflate(R.layout.item_plans, container, false);
        txtPlanName = (TextView) view.findViewById(R.id.txtPlanName);
        txtDisc = (TextView) view.findViewById(R.id.txtDisc);
        container.addView(view);

        return view;
    }

    @Override
    public int getCount() {
        return itemList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object obj) {
        return view == obj;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        View view = (View) object;
        container.removeView(view);
    }
}
