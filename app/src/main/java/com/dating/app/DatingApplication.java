package com.dating.app;

import android.app.Application;
import android.util.Log;

import io.agora.AgoraAPIOnlySignal;

public class DatingApplication extends Application {
    private final String TAG = DatingApplication.class.getSimpleName();


    private static DatingApplication mInstance;
    private AgoraAPIOnlySignal m_agoraAPI;


    public static DatingApplication the() {
        return mInstance;
    }

    public DatingApplication() {
        mInstance = this;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        //setupAgoraEngine();
    }

    /*public AgoraAPIOnlySignal getmAgoraAPI() {
        return m_agoraAPI;
    }*/


    /*private void setupAgoraEngine() {
        String appID = getString(R.string.agora_app_id);

        try {
            m_agoraAPI = AgoraAPIOnlySignal.getInstance(this, appID);


        } catch (Exception e) {
            Log.e(TAG, Log.getStackTraceString(e));

            throw new RuntimeException("NEED TO check rtc sdk init fatal error\n" + Log.getStackTraceString(e));
        }
    }*/


}

