package com.dating.app.paytm;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.dating.app.connection.PostRequestToServer;
import com.dating.app.mycallback.AsyncResponse;
import com.dating.app.mycallback.PaytmCustomResponse;
import com.dating.app.utils.Constants;
import com.dating.app.utils.ServerJson;
import com.dating.app.utils.SharedKeys;
import com.dating.app.utils.SharedPreferenceHelper;
import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class PaytmImpl {
    public PaytmCustomResponse delegate;
    PaytmPGService Service;
    Map<String, String> paramMap;
    private Context context;
    private String checkSum = "";
    private float payAable;

    public PaytmImpl(Context context,float payAable) {
        this.context =context;
        this.payAable =payAable;
        Service = PaytmPGService.getProductionService();
        checkSum();

      /* paramMap.put("CALLBACK_URL", "https://securegw.paytm.in/theia/paytmCallback");
        paramMap.put("CHANNEL_ID", "WAP");
        paramMap.put("CHECKSUMHASH", "yDZks+XoWcQ4YZJ+Iod+f/b/7mi5tcGqqQELPhSLQYjGdUfcUnsegcjlsdW797gnsvy4YrHNV8HSIJmdFN0NgWbNTle8wgKFAnSH14crB3A=");
        paramMap.put("CUST_ID", "test111");
        paramMap.put("INDUSTRY_TYPE_ID", "Retail");
        paramMap.put("MID", "HashEd54248080069819");
        paramMap.put("ORDER_ID", "1523342168787");
        paramMap.put("TXN_AMOUNT", "1");
        paramMap.put("WEBSITE", "TECHweb");*/

       /* paramMap.put("MID" , "WorldP64425807474247");
        paramMap.put("ORDER_ID" , "210lkldfka2a27");
        paramMap.put("CUST_ID" , "mkjNYC1227");
        paramMap.put("INDUSTRY_TYPE_ID" , "Retail");
        paramMap.put("CHANNEL_ID" , "WAP");
        paramMap.put("TXN_AMOUNT" , "1");
        paramMap.put("WEBSITE" , "worldpressplg");
        paramMap.put("CALLBACK_URL" , "https://pguat.paytm.com/paytmchecksum/paytmCheckSumVerify.jsp");*/
    }

   public void executePaytm(){
        PaytmOrder Order = new PaytmOrder((HashMap<String, String>) paramMap);
        Service.initialize(Order, null);

        Service.startPaymentTransaction(context, true, true,
                new PaytmPaymentTransactionCallback() {
                    @Override
                    public void someUIErrorOccurred(String inErrorMessage) {
                        delegate.payResponse(inErrorMessage, false);
                        // Some UI Error Occurred in Payment Gateway Activity.
                        // // This may be due to initialization of views in
                        // Payment Gateway Activity or may be due to //
                        // initialization of webview. // Error Message details
                        // the error occurred.
                    }

					/*@Override
					public void onTransactionSuccess(Bundle inResponse) {
						// After successful transaction this method gets called.
						// // Response bundle contains the merchant response
						// parameters.
						Log.d("LOG", "Payment Transaction is successful " + inResponse);
						Toast.makeText(getApplicationContext(), "Payment Transaction is successful ", Toast.LENGTH_LONG).show();
					}
					@Override
					public void onTransactionFailure(String inErrorMessage,
							Bundle inResponse) {
						// This method gets called if transaction failed. //
						// Here in this case transaction is completed, but with
						// a failure. // Error Message describes the reason for
						// failure. // Response bundle contains the merchant
						// response parameters.
						Log.d("LOG", "Payment Transaction Failed " + inErrorMessage);
						Toast.makeText(getBaseContext(), "Payment Transaction Failed ", Toast.LENGTH_LONG).show();
					}*/

                    @Override
                    public void onTransactionResponse(Bundle inResponse) {
                        Log.d("LOG", "Payment Transaction is successful " + inResponse);
                        delegate.payResponse(inResponse.toString(), true);
                    }

                    @Override
                    public void networkNotAvailable() { // If network is not
                        // available, then this
                        // method gets called.
                    }

                    @Override
                    public void clientAuthenticationFailed(String inErrorMessage) {
                        delegate.payResponse(inErrorMessage, false);
                        // This method gets called if client authentication
                        // failed. // Failure may be due to following reasons //
                        // 1. Server error or downtime. // 2. Server unable to
                        // generate checksum or checksum response is not in
                        // proper format. // 3. Server failed to authenticate
                        // that client. That is value of payt_STATUS is 2. //
                        // Error Message describes the reason for failure.

                    }

                    @Override
                    public void onErrorLoadingWebPage(int iniErrorCode,
                                                      String inErrorMessage, String inFailingUrl) {

                    }

                    // had to be added: NOTE
                    @Override
                    public void onBackPressedCancelTransaction() {
                        delegate.payResponse("Back pressed. Transaction cancelled", false);
                    }

                    @Override
                    public void onTransactionCancel(String inErrorMessage, Bundle inResponse) {
                        Log.d("LOG", "Payment Transaction Failed " + inErrorMessage);
                        delegate.payResponse("Back pressed. Transaction cancelled", false);
                    }

                });
    }


    void checkSum(){
        String json = ServerJson.getInstance().jsonCheckSum(SharedPreferenceHelper.get(SharedKeys.email),"1");
        PostRequestToServer postRequestToServer = new PostRequestToServer(json);
        postRequestToServer.delegate = asyncResponse;
        postRequestToServer.execute(Constants.genratechecksum);
    }


    AsyncResponse asyncResponse = new AsyncResponse() {
        @Override
        public void processFinished(String data) {
            try{
                JSONObject jsonObject = new JSONObject(data);
                if(jsonObject.getString("status").equalsIgnoreCase("true")){
                    JSONObject jsonData = jsonObject.getJSONObject("data");
                    checkSum = jsonData.getString("checksumhash");

                    paramMap = new HashMap<String, String>();
                    paramMap.put("CALLBACK_URL", jsonData.getString("CALLBACK_URL"));
                    paramMap.put("CHANNEL_ID",jsonData.getString("channel_id"));
                    paramMap.put("CHECKSUMHASH",checkSum);
                    paramMap.put("CUST_ID", jsonData.getString("cust_id"));
                    paramMap.put("INDUSTRY_TYPE_ID",jsonData.getString("industry_type_id"));
                    paramMap.put("MID",jsonData.getString("mid") );
                    paramMap.put("ORDER_ID", jsonData.getString("order_id"));
                    paramMap.put("TXN_AMOUNT", ""+jsonData.getInt("txn_amount"));
                    paramMap.put("WEBSITE", jsonData.getString("website"));

                    executePaytm();
                }

            }catch (Exception e){
                Log.d("Data",""+e);
            }
        }
    };


}
