package com.dating.app.paytm;
public class PaytmModel {
    private String CALLBACK_URL="https://securegw.paytm.in/theia/paytmCallback";
    private String CHANNEL_ID="WAP";
    private String INDUSTRY_TYPE_ID="Retail";
    private String MID="TECHOP10964184510936";
    private String WEBSITE="TECHweb";
    private String CHECKSUMHASH;
    private String ORDER_ID;
    private String TXN_AMOUNT;

    public String getTXN_AMOUNT() {
        return TXN_AMOUNT;
    }

    public void setTXN_AMOUNT(String TXN_AMOUNT) {
        this.TXN_AMOUNT = TXN_AMOUNT;
    }

    public String getORDER_ID() {
        return ORDER_ID;
    }

    public void setORDER_ID(String ORDER_ID) {
        this.ORDER_ID = ORDER_ID;
    }

    public String getCHECKSUMHASH() {
        return CHECKSUMHASH;
    }

    public void setCHECKSUMHASH(String CHECKSUMHASH) {
        this.CHECKSUMHASH = CHECKSUMHASH;
    }

    public String getWEBSITE() {
        return WEBSITE;
    }

    public String getMID() {
        return MID;
    }

    public String getINDUSTRY_TYPE_ID() {
        return INDUSTRY_TYPE_ID;
    }

    public String getCHANNEL_ID() {
        return CHANNEL_ID;
    }

    public String getCALLBACK_URL() {
        return CALLBACK_URL;
    }
}
